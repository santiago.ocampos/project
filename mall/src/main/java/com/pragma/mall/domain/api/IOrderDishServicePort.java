package com.pragma.mall.domain.api;

import com.pragma.mall.domain.model.OrderDishModel;
import com.pragma.mall.domain.model.OrderModel;

public interface IOrderDishServicePort {

    OrderDishModel saveOrderDish(OrderDishModel orderDishModel);
}
