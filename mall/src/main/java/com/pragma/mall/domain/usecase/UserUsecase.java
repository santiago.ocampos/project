package com.pragma.mall.domain.usecase;

import com.pragma.mall.domain.api.IRestaurantServicePort;
import com.pragma.mall.domain.api.IUserServicePort;
import com.pragma.mall.domain.spi.IRestaurantPersistencePort;
import com.pragma.mall.domain.spi.IUserPersistencePort;

public class UserUsecase implements IUserServicePort {

    private final IUserPersistencePort iUserPersistencePort;

    public UserUsecase(IUserPersistencePort iUserPersistencePort) {
        this.iUserPersistencePort = iUserPersistencePort;
    }
}
