package com.pragma.mall.domain.spi;

import com.pragma.mall.domain.model.DishModel;

import java.util.List;

public interface IDishPersistencePort {
    DishModel saveDish(DishModel dishModel);

    DishModel updateDish(DishModel dishModel, Long id_dish);

    DishModel getById(Long id_dish);

    List<DishModel> getAllDishesByRestaurant(int pageN, int size, Long restaurantId);
}
