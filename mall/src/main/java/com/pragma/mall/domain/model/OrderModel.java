package com.pragma.mall.domain.model;

import com.pragma.mall.application.dto.reponse.OrderDishesResponseDto;
import com.pragma.mall.infrastructure.out.jpa.entity.RestaurantEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderModel {
    private Long id;

    private Long user_id;

    private Date date;

    private String status;
    private Double pin;

    private RestaurantModel restaurant;

    private List<OrderDishModel> dishes;
}
