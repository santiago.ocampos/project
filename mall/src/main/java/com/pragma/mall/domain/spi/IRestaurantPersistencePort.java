package com.pragma.mall.domain.spi;

import com.pragma.mall.domain.model.RestaurantModel;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IRestaurantPersistencePort {
    RestaurantModel saveRestaurant(RestaurantModel restaurantModel);

    public RestaurantModel getById(Long restaurat_id);

    List<RestaurantModel> getRestaurants(int pageNow, int siz);

}
