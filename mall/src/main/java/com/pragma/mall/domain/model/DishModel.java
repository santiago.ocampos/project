package com.pragma.mall.domain.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DishModel {

    private Long id;


    private String name;


    private String description;


    private float price;


    private String url;

    private String file;


    private boolean status;

    private CategoryModel category;

    private RestaurantModel restaurant;

}
