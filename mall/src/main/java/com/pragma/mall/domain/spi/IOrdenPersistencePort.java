package com.pragma.mall.domain.spi;

import com.pragma.mall.domain.model.OrderDishModel;
import com.pragma.mall.domain.model.OrderModel;
import com.pragma.mall.domain.model.RestaurantModel;

import java.util.List;

public interface IOrdenPersistencePort {

    OrderModel saveOrder(OrderModel orderModel);


    OrderModel getById(Long order_id);

    List<OrderModel> getAllOrdersByStatus(int pageN, int size, String status);

    OrderModel assignOrder(OrderModel orderModel);

}
