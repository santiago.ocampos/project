package com.pragma.mall.domain.spi;

import com.pragma.mall.domain.model.OrderDishModel;

public interface IOrdenDishPersistencePort {

    OrderDishModel saveOrderDish(OrderDishModel orderDishModel);
}
