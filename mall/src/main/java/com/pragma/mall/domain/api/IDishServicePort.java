package com.pragma.mall.domain.api;

import com.pragma.mall.domain.model.DishModel;

import java.util.List;

public interface IDishServicePort {


    DishModel saveDish(DishModel dishModel);

    DishModel getById(Long dish_id);


    DishModel updateDish(DishModel dishModel, Long dish_id);

    List<DishModel> getAllDishesByRestaurant(int pageN, int size, Long restaurantId);
}
