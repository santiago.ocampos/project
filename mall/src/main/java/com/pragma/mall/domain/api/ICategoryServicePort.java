package com.pragma.mall.domain.api;

import com.pragma.mall.domain.model.CategoryModel;

import java.util.List;

public interface ICategoryServicePort {

    CategoryModel getById(Long category_id);

    List<CategoryModel> getAllCategories();
}
