package com.pragma.mall.domain.usecase;

import com.pragma.mall.domain.api.IOrderDishServicePort;
import com.pragma.mall.domain.model.OrderDishModel;
import com.pragma.mall.domain.spi.IOrdenDishPersistencePort;

public class OrderDishUseCase implements IOrderDishServicePort {

    private final IOrdenDishPersistencePort ordenDishPersistencePort;

    public OrderDishUseCase(IOrdenDishPersistencePort ordenDishPersistencePort) {
        this.ordenDishPersistencePort = ordenDishPersistencePort;
    }


    @Override
    public OrderDishModel saveOrderDish(OrderDishModel orderDishModel) {
        return this.ordenDishPersistencePort.saveOrderDish(orderDishModel);
    }
}
