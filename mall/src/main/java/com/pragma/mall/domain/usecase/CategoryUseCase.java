package com.pragma.mall.domain.usecase;

import com.pragma.mall.domain.api.ICategoryServicePort;
import com.pragma.mall.domain.model.CategoryModel;
import com.pragma.mall.domain.spi.ICategoryPersistencePort;

import java.util.List;

public class CategoryUseCase implements ICategoryServicePort {

    private final ICategoryPersistencePort categoryPersistencePort;


    public CategoryUseCase(ICategoryPersistencePort rolePersistencePort) {
        this.categoryPersistencePort = rolePersistencePort;
    }

    @Override
    public CategoryModel getById(Long category_id) {
        return categoryPersistencePort.getById(category_id);
    }

    @Override
    public List<CategoryModel> getAllCategories() {
        return categoryPersistencePort.getAllCategories();
    }
}
