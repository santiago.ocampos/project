package com.pragma.mall.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantModel {

    private Long id;

    private String name;

    private String address;

    private String phone;

    private String url;

    private String nit;

    private Long user_id;
}
