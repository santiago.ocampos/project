package com.pragma.mall.domain.spi;

import com.pragma.mall.domain.model.CategoryModel;

import java.util.List;

public interface ICategoryPersistencePort {

    CategoryModel getById(Long category_id);

    List<CategoryModel> getAllCategories();
}
