package com.pragma.mall.domain.api;

import com.pragma.mall.domain.model.RestaurantModel;

import java.util.List;

public interface IRestaurantServicePort {
    RestaurantModel saveRestaurant(RestaurantModel restaurantModel);
    RestaurantModel getById(Long restaurant_id);

    List<RestaurantModel> getRestaurants(int pageNow, int size);
}
