package com.pragma.mall.domain.usecase;

import com.pragma.mall.domain.api.IOrderServicePort;
import com.pragma.mall.domain.model.OrderDishModel;
import com.pragma.mall.domain.model.OrderModel;
import com.pragma.mall.domain.model.RestaurantModel;
import com.pragma.mall.domain.spi.IOrdenPersistencePort;


import java.util.List;

public class OrderUseCase implements IOrderServicePort {

    private final IOrdenPersistencePort orderPersistencePort;

    public OrderUseCase(IOrdenPersistencePort orderPersistencePort) {
        this.orderPersistencePort = orderPersistencePort;
    }

    @Override
    public OrderModel saveOrder(OrderModel orderModel) {
        return orderPersistencePort.saveOrder(orderModel);
    }

    @Override
    public OrderModel getById(Long order_id) {
        return orderPersistencePort.getById(order_id);
    }

    @Override
    public List<OrderModel> getAllOrdersByStatus(int pageN, int size, String status) {
        return orderPersistencePort.getAllOrdersByStatus( pageN,size, status);
    }

    @Override
    public OrderModel assignOrder(OrderModel orderModel) {
        return orderPersistencePort.assignOrder(orderModel);
    }

}
