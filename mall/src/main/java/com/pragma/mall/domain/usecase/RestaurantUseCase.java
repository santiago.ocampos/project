package com.pragma.mall.domain.usecase;

import com.pragma.mall.application.dto.reponse.RestaurantResponseDto;
import com.pragma.mall.domain.api.IRestaurantServicePort;
import com.pragma.mall.domain.model.RestaurantModel;
import com.pragma.mall.domain.spi.IRestaurantPersistencePort;
import org.springframework.data.domain.Pageable;

import java.util.List;

public class RestaurantUseCase implements IRestaurantServicePort {


    private final IRestaurantPersistencePort restaurantPersistencePort;


    public RestaurantUseCase(IRestaurantPersistencePort restaurantPersistencePort) {
        this.restaurantPersistencePort = restaurantPersistencePort;
    }

    @Override
    public RestaurantModel saveRestaurant(RestaurantModel restaurantModel) {
        return restaurantPersistencePort.saveRestaurant(restaurantModel);
    }

    @Override
    public RestaurantModel getById(Long restaurant_id) {
        return restaurantPersistencePort.getById(restaurant_id);
    }

    @Override
    public List<RestaurantModel> getRestaurants(int pageNow, int size) {
        return restaurantPersistencePort.getRestaurants(pageNow,size);
    }
}
