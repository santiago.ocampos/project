package com.pragma.mall.domain.usecase;

import com.pragma.mall.domain.api.IDishServicePort;
import com.pragma.mall.domain.model.DishModel;
import com.pragma.mall.domain.spi.IDishPersistencePort;

import java.util.List;

public class DishUseCase implements IDishServicePort {


    private final IDishPersistencePort dishPersistencePort;


    public DishUseCase(IDishPersistencePort dishPersistencePort) {
        this.dishPersistencePort = dishPersistencePort;
    }

    ;

    @Override
    public DishModel saveDish(DishModel dishModel) {
        return dishPersistencePort.saveDish(dishModel);
    }

    @Override
    public DishModel getById(Long dish_id) {
        return dishPersistencePort.getById(dish_id);
    }


    @Override
    public DishModel updateDish(DishModel dishModel, Long dish_id) {
        return dishPersistencePort.updateDish(dishModel,dish_id);
    }

    @Override
    public List<DishModel> getAllDishesByRestaurant(int pageN, int size, Long restaurantId) {
        return dishPersistencePort.getAllDishesByRestaurant(pageN, size, restaurantId);
    }
}
