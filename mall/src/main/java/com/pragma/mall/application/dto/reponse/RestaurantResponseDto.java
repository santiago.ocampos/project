package com.pragma.mall.application.dto.reponse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestaurantResponseDto {

    private Long id;
    private String name;

    private String address;

    private String phone;

    private String url;

    private String nit;

    private Long user_id;
}
