package com.pragma.mall.application.handler.impl;
import com.pragma.mall.application.dto.reponse.RestaurantResponseDto;
import com.pragma.mall.application.dto.request.ListPaginationRequest;
import com.pragma.mall.application.dto.request.RestaurantRequestDto;
import com.pragma.mall.application.dto.request.UserRequestDto;
import com.pragma.mall.application.handler.IRestaurantHandler;
import com.pragma.mall.application.mapper.IRestaurantRequestMapper;
import com.pragma.mall.domain.api.IRestaurantServicePort;
import com.pragma.mall.domain.model.RestaurantModel;
import com.pragma.mall.infrastructure.configuration.FeignClientInterceptor;
import com.pragma.mall.infrastructure.exception.NoDataFoundException;
import com.pragma.mall.infrastructure.input.rest.client.IUserClient;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@RequiredArgsConstructor
@Transactional
public class RestaurantHandler implements IRestaurantHandler {
    private final IRestaurantServicePort restaurantServicePort;
    private final IRestaurantRequestMapper restaurantRequestMapper;

    private final IUserClient userRepository;



    @Override
    public RestaurantResponseDto saveRestaurant(RestaurantRequestDto restaurantRequestDto) {
        RestaurantModel restaurantModel = restaurantRequestMapper.toRestaurant(restaurantRequestDto);

        String token=FeignClientInterceptor.getBearerTokenHeader();
        UserRequestDto user = userRepository.getUserById(token,restaurantModel.getUser_id()).getBody().getData();

        if(user==null){
            throw new NoDataFoundException();
        }
        return restaurantRequestMapper.toDto(restaurantServicePort.saveRestaurant(restaurantModel));
    }

    @Override
    public List<RestaurantResponseDto> getRestaurants(ListPaginationRequest listPaginationRequest) {
        return restaurantRequestMapper.listToDto(restaurantServicePort.getRestaurants(listPaginationRequest.getPageN(), listPaginationRequest.getSize()));
    }
}
