package com.pragma.mall.application.dto.reponse;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DishResponseDto {

    private String name;

    private String description;



    private Float price;


    private String url;



    private String file;

    private boolean status;



    private CategoryResponseDto category;


    private RestaurantResponseDto restaurant;
}
