package com.pragma.mall.application.handler.impl;

import com.pragma.mall.application.dto.reponse.DishCategoryResponseDto;
import com.pragma.mall.application.dto.reponse.DishResponseDto;
import com.pragma.mall.application.dto.request.ListPaginationRequest;
import com.pragma.mall.application.dto.request.UserRequestDto;
import com.pragma.mall.application.dto.request.dish.DishRequestDto;
import com.pragma.mall.application.dto.request.dish.DishUpdateDto;
import com.pragma.mall.application.handler.IDishHandler;
import com.pragma.mall.application.mapper.IDishRequestMapper;
import com.pragma.mall.application.mapper.IDishResponseMapper;
import com.pragma.mall.domain.api.ICategoryServicePort;
import com.pragma.mall.domain.api.IDishServicePort;
import com.pragma.mall.domain.api.IRestaurantServicePort;
import com.pragma.mall.domain.model.CategoryModel;
import com.pragma.mall.domain.model.DishModel;
import com.pragma.mall.domain.model.RestaurantModel;
import com.pragma.mall.infrastructure.configuration.FeignClientInterceptor;
import com.pragma.mall.infrastructure.exception.NoDataFoundException;
import com.pragma.mall.infrastructure.exception.NoOwnerException;
import com.pragma.mall.infrastructure.input.rest.client.IUserClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class DishHandler implements IDishHandler {


    private final IDishServicePort dishServicePort;
    private final ICategoryServicePort categoryServicePort;

    private final IRestaurantServicePort restaurantServicePort;

    private final IDishResponseMapper dishResponseMapper;


    private final IDishRequestMapper dishRequestMapper;


    private final IUserClient userRepository;

    @Override
    public DishResponseDto saveDish(DishRequestDto dishRequestDto) {
        CategoryModel category=categoryServicePort.getById(dishRequestDto.getCategory_id());
        RestaurantModel restaurant=restaurantServicePort.getById(dishRequestDto.getRestaurant_id());
        String token= FeignClientInterceptor.getBearerTokenHeader();
        UserRequestDto user = userRepository.getInfo(token).getBody().getData();
        if (user.getId()!=restaurant.getUser_id()){
            throw new NoOwnerException();
        }
        DishModel dishModel = dishRequestMapper.toDish(dishRequestDto);
        dishModel.setStatus(true);
        dishModel.setCategory(category);
        dishModel.setRestaurant(restaurant);
        return dishResponseMapper.toDto(dishServicePort.saveDish(dishModel));
    }

    @Override
    public DishResponseDto updateDish(DishUpdateDto dishUpdateRequestDto, Long dish_id) {
        DishModel dishModel = dishServicePort.getById(dish_id);
        String token= FeignClientInterceptor.getBearerTokenHeader();
        UserRequestDto user = userRepository.getInfo(token).getBody().getData();
        if (user.getId()!=dishModel.getRestaurant().getUser_id()){
            throw new NoOwnerException();
        }
        DishModel newdishModel = dishRequestMapper.toUpdateDish(dishUpdateRequestDto);
        return dishResponseMapper.toDto(dishServicePort.updateDish(newdishModel, dish_id));
    }
    @Override
    public DishResponseDto changeStatusDish(Long dish_id) {
        DishModel dishModel = dishServicePort.getById(dish_id);
        String token= FeignClientInterceptor.getBearerTokenHeader();
        UserRequestDto user = userRepository.getInfo(token).getBody().getData();
        if (user.getId()!=dishModel.getRestaurant().getUser_id()){
            throw new NoOwnerException();
        }
        if(dishModel.isStatus()==true){
            dishModel.setStatus(false);
        }else{
            dishModel.setStatus(true);
        }
        return dishResponseMapper.toDto(dishServicePort.updateDish(dishModel, dish_id));
    }

    @Override
    public List<DishCategoryResponseDto> getDishesByRestaurant(ListPaginationRequest listPaginationRequest, Long restaurant_Id) {
        return dishResponseMapper.toDishCategoryList(dishServicePort.getAllDishesByRestaurant(listPaginationRequest.getPageN(), listPaginationRequest.getSize(), restaurant_Id), categoryServicePort.getAllCategories());

    }
}
