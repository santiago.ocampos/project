package com.pragma.mall.application.handler;

import com.pragma.mall.application.dto.reponse.RestaurantResponseDto;
import com.pragma.mall.application.dto.request.ListPaginationRequest;
import com.pragma.mall.application.dto.request.RestaurantRequestDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IRestaurantHandler {


    RestaurantResponseDto saveRestaurant(RestaurantRequestDto restaurantRequestDto);

    List<RestaurantResponseDto> getRestaurants(ListPaginationRequest listPaginationRequest);
}
