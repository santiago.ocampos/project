package com.pragma.mall.application.dto.reponse;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDishesResponseDto {

    private DishResponseDto dish;

    private int amount;
}
