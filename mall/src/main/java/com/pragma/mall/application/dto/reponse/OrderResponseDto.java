package com.pragma.mall.application.dto.reponse;

import com.pragma.mall.infrastructure.out.jpa.entity.RestaurantEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class OrderResponseDto {

    private Long id;

    private Long user_id;


    private Date date;

    private String status;

    private RestaurantResponseDto restaurant;

    private List<OrderDishesResponseDto> dishes;
}
