package com.pragma.mall.application.dto.reponse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryResponseDto {


    private long id;
    private String name;
    private String description;
}
