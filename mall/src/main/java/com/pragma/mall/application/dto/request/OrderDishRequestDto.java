package com.pragma.mall.application.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class OrderDishRequestDto {

    @NotNull(message ="El id del plato es obligatorio")
    private Long dish_id;

    @NotNull(message ="La cantidad debe ser obligatoria")
    private int amount;
}
