package com.pragma.mall.application.dto.request.dish;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Getter
@Setter
public class DishUpdateDto {

    @NotNull(message ="El campo precio es obligatorio")
    @Min(value=0, message="El precio mínimo es 0")
    private Float price;


    @NotNull(message ="El campo descripción es obligatorio")
    private String description;

}
