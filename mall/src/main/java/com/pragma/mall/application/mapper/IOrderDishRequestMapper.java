package com.pragma.mall.application.mapper;

import com.pragma.mall.application.dto.request.OrderDishRequestDto;
import com.pragma.mall.application.dto.request.RestaurantRequestDto;
import com.pragma.mall.domain.model.OrderDishModel;
import com.pragma.mall.domain.model.RestaurantModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IOrderDishRequestMapper {
    OrderDishModel toOrderDish(OrderDishRequestDto orderDishRequestDto);

}
