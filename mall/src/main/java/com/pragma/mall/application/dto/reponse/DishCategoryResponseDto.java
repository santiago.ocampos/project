package com.pragma.mall.application.dto.reponse;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DishCategoryResponseDto {
    private String name;
    private String description;

    private List<DishResponseDto> dishes;
}
