package com.pragma.mall.application.handler;

import com.pragma.mall.application.dto.reponse.DishCategoryResponseDto;
import com.pragma.mall.application.dto.reponse.OrderResponseDto;
import com.pragma.mall.application.dto.request.ListPaginationRequest;
import com.pragma.mall.application.dto.request.OrderDishRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface IOrderHandler {

    OrderResponseDto saveOrder(List<OrderDishRequestDto> orderDishRequestDtos, Long restaurant_Id);

    OrderResponseDto getbyId(Long order_id);
    List<OrderResponseDto> getAllOrdersByStatus(ListPaginationRequest listPaginationRequest, String status);


    OrderResponseDto assignOrder(Long order_id, Double pin);
}
