package com.pragma.mall.application.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PinRequestDto {


    Double pin;
}
