package com.pragma.mall.application.mapper;


import com.pragma.mall.application.dto.reponse.RestaurantResponseDto;
import com.pragma.mall.application.dto.request.RestaurantRequestDto;
import com.pragma.mall.domain.model.RestaurantModel;
import org.mapstruct.Mapper;

import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IRestaurantRequestMapper {
    RestaurantModel toRestaurant(RestaurantRequestDto restaurantRequestDto);

    RestaurantResponseDto toDto(RestaurantModel userModel);

    List<RestaurantResponseDto> listToDto(List<RestaurantModel> listUserModel);

}
