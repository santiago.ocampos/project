package com.pragma.mall.application.mapper;

import com.pragma.mall.application.dto.reponse.DishCategoryResponseDto;
import com.pragma.mall.application.dto.reponse.OrderDishesResponseDto;
import com.pragma.mall.application.dto.reponse.OrderResponseDto;
import com.pragma.mall.application.dto.reponse.RestaurantResponseDto;
import com.pragma.mall.domain.model.*;
import org.hibernate.criterion.Order;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IOrderDishResponseMapper {

    List<OrderDishesResponseDto> toRestaurantDto(List<OrderDishModel> orderDishModelList);


    IRestaurantRequestMapper RESTAURANT_RESPONSE_MAPPER_INSTANCE = Mappers.getMapper(IRestaurantRequestMapper.class);
    default OrderResponseDto  toDishCategoryList(OrderModel orderModel, List<OrderDishModel> orderDishModelList) {

        OrderResponseDto orderResponseDto=new OrderResponseDto();
        orderResponseDto.setDate(orderModel.getDate());
        orderResponseDto.setUser_id(orderModel.getUser_id());
        orderResponseDto.setStatus(orderModel.getStatus());
        orderResponseDto.setRestaurant(RESTAURANT_RESPONSE_MAPPER_INSTANCE.toDto(orderModel.getRestaurant()));
        orderResponseDto.setDishes(toRestaurantDto(orderDishModelList));

        return orderResponseDto;
    }
    OrderResponseDto toOrderModelToOrderResponse(OrderModel orderModel);

    List<OrderResponseDto> toListOrderModelToListOrderResponse(List<OrderModel> orderModel);
}
