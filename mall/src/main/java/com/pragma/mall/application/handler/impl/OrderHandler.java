package com.pragma.mall.application.handler.impl;

import com.pragma.mall.application.dto.reponse.DishCategoryResponseDto;
import com.pragma.mall.application.dto.reponse.OrderResponseDto;
import com.pragma.mall.application.dto.reponse.ResponseDto;
import com.pragma.mall.application.dto.request.ListPaginationRequest;
import com.pragma.mall.application.dto.request.OrderDishRequestDto;
import com.pragma.mall.application.dto.request.TwilioRequestDto;
import com.pragma.mall.application.dto.request.UserRequestDto;
import com.pragma.mall.application.handler.IOrderHandler;
import com.pragma.mall.application.mapper.IOrderDishRequestMapper;
import com.pragma.mall.application.mapper.IOrderDishResponseMapper;
import com.pragma.mall.application.mapper.IRestaurantRequestMapper;
import com.pragma.mall.domain.api.IDishServicePort;
import com.pragma.mall.domain.api.IOrderDishServicePort;
import com.pragma.mall.domain.api.IOrderServicePort;
import com.pragma.mall.domain.api.IRestaurantServicePort;
import com.pragma.mall.domain.model.DishModel;
import com.pragma.mall.domain.model.OrderDishModel;
import com.pragma.mall.domain.model.OrderModel;
import com.pragma.mall.domain.model.RestaurantModel;
import com.pragma.mall.infrastructure.configuration.FeignClientInterceptor;
import com.pragma.mall.infrastructure.exception.NoDataFoundException;
import com.pragma.mall.infrastructure.exception.NoOwnerException;
import com.pragma.mall.infrastructure.exception.NoPinException;
import com.pragma.mall.infrastructure.input.rest.client.ITwilioClient;
import com.pragma.mall.infrastructure.input.rest.client.IUserClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderHandler implements IOrderHandler {

    private final IRestaurantServicePort restaurantServicePort;
    private final IDishServicePort dishServicePort;
    private final IOrderServicePort orderServicePort;
    private final IOrderDishServicePort orderDishServicePort;

    private final IOrderDishRequestMapper orderDishRequestMapper;

    private final IOrderDishResponseMapper orderDishResponseMapper;
    private final IUserClient userRepository;


    private final ITwilioClient iTwilioClient;
    @Override
    public OrderResponseDto saveOrder(List<OrderDishRequestDto> orderDishRequestDtos, Long restaurant_Id) {
        RestaurantModel restaurant=restaurantServicePort.getById(restaurant_Id);
        String token= FeignClientInterceptor.getBearerTokenHeader();
        UserRequestDto user = userRepository.getInfo(token).getBody().getData();

        if(user==null){
            throw new NoDataFoundException();
        }
        OrderModel orderModel=new OrderModel();
        orderModel.setDate(new Date());
        orderModel.setStatus("PENDIENTE");
        orderModel.setRestaurant(restaurant);
        orderModel.setUser_id(user.getId());
        orderModel=orderServicePort.saveOrder(orderModel);
        List<OrderDishModel> orderDishModelList=new ArrayList<>();
        for(OrderDishRequestDto orderDishRequestDto:orderDishRequestDtos) {
            DishModel dishModel=dishServicePort.getById(orderDishRequestDto.getDish_id());
            OrderDishModel orderDishModel=orderDishRequestMapper.toOrderDish(orderDishRequestDto);
            orderDishModel.setDish(dishModel);
            orderDishModel.setOrder(orderModel);
            if(orderDishModel.getDish().getRestaurant().getId()==restaurant.getId()) {
                orderDishServicePort.saveOrderDish(orderDishModel);
                orderDishModelList.add(orderDishModel);
            }
        }
        return orderDishResponseMapper.toDishCategoryList(orderModel,orderDishModelList);
    }
    @Override
    public OrderResponseDto getbyId(Long order_id) {
        OrderModel orderModel=orderServicePort.getById(order_id);
        return orderDishResponseMapper.toOrderModelToOrderResponse(orderModel);
    }
    @Override
    public List<OrderResponseDto> getAllOrdersByStatus(ListPaginationRequest listPaginationRequest, String status) {
        return orderDishResponseMapper.toListOrderModelToListOrderResponse(orderServicePort.getAllOrdersByStatus(listPaginationRequest.getPageN(), listPaginationRequest.getSize(), status));

    }

    @Override
    public OrderResponseDto assignOrder(Long order_id,Double pin) {
        OrderModel orderModel=orderServicePort.getById(order_id);
        String token= FeignClientInterceptor.getBearerTokenHeader();
        UserRequestDto user = userRepository.getInfo(token).getBody().getData();

        if(orderModel.getStatus().equals("PENDIENTE")){
            orderModel.setStatus("EN_PREPARACIÓN");
            orderModel.setUser_id(user.getId());
            return orderDishResponseMapper.toOrderModelToOrderResponse(orderServicePort.assignOrder(orderModel));

        }else if((orderModel.getStatus().equals("EN_PREPARACIÓN"))){
            double fiveDigits = 10000 + Math.random() * 90000;
            orderModel.setPin(fiveDigits);
            orderModel.setStatus("LISTO");
            TwilioRequestDto twilioRequestDto=new TwilioRequestDto();
            twilioRequestDto.setNumber("+573188230963");
            twilioRequestDto.setMessage("Tu pedido esta listo puedes reclamarlo con el siguiente pin:" +fiveDigits);
            ResponseDto responseDto= iTwilioClient.sendMessage(token,twilioRequestDto).getBody();
            return orderDishResponseMapper.toOrderModelToOrderResponse(orderServicePort.assignOrder(orderModel));

        }else if((orderModel.getStatus().equals("LISTO"))){
            if(orderModel.getPin()==pin){
                orderModel.setStatus("ENTREGADO");
                return orderDishResponseMapper.toOrderModelToOrderResponse(orderServicePort.assignOrder(orderModel));
            }else{
                throw new NoPinException();
            }
        }
        return null;


    }
}
