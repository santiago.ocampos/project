package com.pragma.mall.application.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;


@Getter
@Setter
public class RestaurantRequestDto {

    @NotNull(message ="El campo nombre es obligatorio")
    private String name;

    @NotNull(message ="El campo dirección es obligatorio")
    private String address;

    @NotNull(message ="El campo telefono es obligatorio")
    private String phone;

    @NotNull(message ="El campo url de la imagen es obligatorio")
    private String url;

    @NotNull(message ="El campo nit es obligatorio")
    private String nit;

    @NotNull(message ="El id del propietario es obligatorio")
    private Long user_id;
}
