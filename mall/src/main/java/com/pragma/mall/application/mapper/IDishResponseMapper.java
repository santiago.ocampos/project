package com.pragma.mall.application.mapper;

import com.pragma.mall.application.dto.reponse.DishCategoryResponseDto;
import com.pragma.mall.application.dto.reponse.DishResponseDto;
import com.pragma.mall.domain.model.CategoryModel;
import com.pragma.mall.domain.model.DishModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IDishResponseMapper {

    DishResponseDto toDto(DishModel dishModel);

    List<DishResponseDto> toListDto(List<DishModel> listDishModel);
    default List<DishCategoryResponseDto> toDishCategoryList(List<DishModel> dishModelList, List<CategoryModel> categoryModelList) {
        return categoryModelList.stream()
                .map(category -> {
                    DishCategoryResponseDto dishCategoryResponseDto = new DishCategoryResponseDto();

                    dishCategoryResponseDto.setName(category.getName());
                    dishCategoryResponseDto.setDescription(category.getDescription());

                    dishCategoryResponseDto.setDishes(toListDto(dishModelList));


                    return dishCategoryResponseDto;
                }).toList();
    }
}
