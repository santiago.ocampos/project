package com.pragma.mall.application.handler;

import com.pragma.mall.application.dto.reponse.DishCategoryResponseDto;
import com.pragma.mall.application.dto.reponse.DishResponseDto;
import com.pragma.mall.application.dto.request.ListPaginationRequest;
import com.pragma.mall.application.dto.request.dish.DishRequestDto;
import com.pragma.mall.application.dto.request.dish.DishUpdateDto;

import java.util.List;

public interface IDishHandler {

    DishResponseDto saveDish(DishRequestDto dishRequestDto);

    DishResponseDto updateDish(DishUpdateDto dishUpdateRequestDto, Long dish_id);

    DishResponseDto changeStatusDish(Long dish_id);

    List<DishCategoryResponseDto> getDishesByRestaurant(ListPaginationRequest listPaginationRequest, Long restaurant_Id);
}
