package com.pragma.mall.application.dto.request.dish;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DishRequestDto {


    @NotNull(message ="El campo nombre es obligatorio")
    private String name;

    @NotNull(message ="El campo descripción es obligatorio")
    private String description;


    @NotNull(message ="El campo precio es obligatorio")
    @Min(value=0, message="El precio mínimo es 0")
    private Float price;

    @NotNull(message ="El campo url es obligatorio")
    private String url;


    @NotNull(message ="El campo archivo es obligatorio")
    private String file;

    @NotNull(message ="El campo categoría es obligatorio")
    private Long category_id;


    @NotNull(message ="El campo restaurante es obligatorio")
    private Long restaurant_id;
}
