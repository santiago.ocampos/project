package com.pragma.mall.application.mapper;


import com.pragma.mall.application.dto.request.RestaurantRequestDto;
import com.pragma.mall.application.dto.request.UserRequestDto;
import com.pragma.mall.domain.model.UserModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IUserRequestMapper {
    UserModel toUser(UserRequestDto userRequestDto);
}
