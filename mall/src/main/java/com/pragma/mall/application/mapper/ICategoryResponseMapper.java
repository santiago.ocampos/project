package com.pragma.mall.application.mapper;

import com.pragma.mall.application.dto.reponse.CategoryResponseDto;
import com.pragma.mall.domain.model.CategoryModel;

import java.util.List;

public interface ICategoryResponseMapper {
    CategoryResponseDto toResponse(CategoryModel categoryModel);

    List<CategoryResponseDto> toResponseList(List<CategoryModel> categoryModels);
}
