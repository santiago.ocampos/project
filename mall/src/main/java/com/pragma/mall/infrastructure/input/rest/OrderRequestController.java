package com.pragma.mall.infrastructure.input.rest;


import com.pragma.mall.application.dto.reponse.DishCategoryResponseDto;
import com.pragma.mall.application.dto.reponse.OrderResponseDto;
import com.pragma.mall.application.dto.reponse.ResponseDto;
import com.pragma.mall.application.dto.reponse.RestaurantResponseDto;
import com.pragma.mall.application.dto.request.ListPaginationRequest;
import com.pragma.mall.application.dto.request.OrderDishRequestDto;
import com.pragma.mall.application.dto.request.PinRequestDto;
import com.pragma.mall.application.dto.request.RestaurantRequestDto;
import com.pragma.mall.application.handler.IOrderHandler;
import com.pragma.mall.application.handler.IRestaurantHandler;
import com.pragma.mall.infrastructure.exception.NoDataFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/order")
@RequiredArgsConstructor
public class OrderRequestController {

    private final IOrderHandler orderHandler;


    @Operation(summary = "H11 Realizar pedido")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Pedido", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResponseDto.class)) }),
    })
    @RolesAllowed({"ROLE_CLIENTE"})
    @PostMapping("/{id_restaurant}")
    public ResponseEntity<ResponseDto> saveOrder(@Valid @RequestBody List<OrderDishRequestDto> orderDishRequestDtos, BindingResult bindingResult,@PathVariable Long id_restaurant) {

        ResponseDto responseDto=new ResponseDto();

        if (bindingResult.hasErrors()) {
            List<String> errors = bindingResult.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());

            responseDto.setError(true);
            responseDto.setMessage("Error en las validaciones");
            responseDto.setData(errors);

            return ResponseEntity.badRequest().body(responseDto);
        }

        try {
            OrderResponseDto restaurantResponseDto= orderHandler.saveOrder(orderDishRequestDtos,id_restaurant);
            responseDto.setError(true);
            responseDto.setData(restaurantResponseDto);
        }catch (NoDataFoundException ex){
            responseDto.setError(true);
            responseDto.setMessage("Usuario no existente");
            responseDto.setData(null);
        }catch (Exception e){
            responseDto.setError(true);
            responseDto.setMessage("Error interno en el servidor");
            responseDto.setData(null);
        }

        return ResponseEntity.ok(responseDto);
    }
    @RolesAllowed({"ROLE_CLIENTE"})
    @GetMapping("/obtener/{id}")
    public ResponseEntity<ResponseDto> getbyId(@PathVariable Long id) {

        ResponseDto responseDto=new ResponseDto();

        try {
            OrderResponseDto restaurantResponseDto= orderHandler.getbyId(id);
            responseDto.setError(true);
            responseDto.setData(restaurantResponseDto);
        }catch (NoDataFoundException ex){
            responseDto.setError(true);
            responseDto.setMessage("Usuario no existente");
            responseDto.setData(null);
        }catch (Exception e){
            responseDto.setError(true);
            responseDto.setMessage("Error interno en el servidor");
            responseDto.setData(null);
        }

        return ResponseEntity.ok(responseDto);
    }

    @Operation(summary = "H12 Lista de pedidos filtrado por estado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Lista de pedidos", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResponseDto.class)) }),
    })
    @RolesAllowed({"ROLE_EMPLEADO"})
    @GetMapping("/orders/{status}")
    public ResponseEntity<ResponseDto> getAllDishesByRestaurantId(@Valid @RequestBody ListPaginationRequest listPaginationRequest,
                                                                  @PathVariable String status,
                                                                  BindingResult bindingResult) {
        ResponseDto responseDto = new ResponseDto();
        if (bindingResult.hasErrors()) {
            List<String> errors = bindingResult.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());

            responseDto.setError(true);
            responseDto.setMessage("Error en las validaciones");
            responseDto.setData(errors);

            return ResponseEntity.badRequest().body(responseDto);
        }

        try {
            List<OrderResponseDto> orderResponseDtos = orderHandler.getAllOrdersByStatus(listPaginationRequest, status);
            responseDto.setError(false);
            responseDto.setMessage(null);
            responseDto.setData(orderResponseDtos);

        } catch (NoDataFoundException ex) {
            responseDto.setError(true);
            responseDto.setMessage("No se encontró el restaurante");
            responseDto.setData(null);
            return new ResponseEntity<>(responseDto, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }


    @Operation(summary = "H13,14,15 Cambiar Estado del empleado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Pedido", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResponseDto.class)) }),
    })
    @RolesAllowed({"ROLE_EMPLEADO"})
    @PutMapping("/assign/{id}")
    public ResponseEntity<ResponseDto> assignOrder(@RequestBody PinRequestDto pinRequestDto, @PathVariable Long id) {
        ResponseDto responseDto = new ResponseDto();
        try {
            OrderResponseDto orderResponseDto = orderHandler.assignOrder(id,pinRequestDto.getPin());
            responseDto.setError(false);
            responseDto.setMessage(null);
            responseDto.setData(orderResponseDto);

        } catch (NoDataFoundException ex) {
            responseDto.setError(true);
            responseDto.setMessage("No se encontró el restaurante");
            responseDto.setData(null);
            return new ResponseEntity<>(responseDto, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }
}
