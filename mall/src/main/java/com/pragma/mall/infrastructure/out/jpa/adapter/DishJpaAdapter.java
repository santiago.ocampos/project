package com.pragma.mall.infrastructure.out.jpa.adapter;

import com.pragma.mall.domain.model.DishModel;
import com.pragma.mall.domain.spi.IDishPersistencePort;
import com.pragma.mall.infrastructure.exception.NoDataFoundException;
import com.pragma.mall.infrastructure.out.jpa.entity.DishEntity;
import com.pragma.mall.infrastructure.out.jpa.mapper.IDishEntityMapper;
import com.pragma.mall.infrastructure.out.jpa.repository.IDishRepositiry;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;


@RequiredArgsConstructor
public class DishJpaAdapter implements IDishPersistencePort {


    private final IDishRepositiry dishRepository;
    private final IDishEntityMapper dishEntityMapper;
    @Override
    public DishModel saveDish(DishModel dishModel) {
        DishEntity dishEntity = dishRepository.save(dishEntityMapper.toEntity(dishModel));
        return dishEntityMapper.toDishModel(dishEntity);
    }

    @Override
    public DishModel updateDish(DishModel dishModel, Long id_dish) {

        DishEntity dishEntity= dishRepository.findById(id_dish).map(dish -> {
            dish.setPrice(dishModel.getPrice());
            dish.setStatus(dishModel.isStatus());
            dish.setDescription(dishModel.getDescription());
            return dishRepository.save(dish);
        }).orElseThrow(() -> new NoDataFoundException());

        return dishEntityMapper.toDishModel(dishEntity);
    }

    @Override
    public DishModel getById(Long id_dish) {

        DishEntity dishEntity= dishRepository.findById(id_dish).orElseThrow(() -> new NoDataFoundException());

        return dishEntityMapper.toDishModel(dishEntity);
    }

    @Override
    public List<DishModel> getAllDishesByRestaurant(int pageN, int size, Long restaurantId) {
        Pageable pagingSort = PageRequest.of(pageN, size, Sort.by("category.name"));
        Page<DishEntity> page = dishRepository.findAllByRestaurantId(restaurantId, pagingSort);
        List<DishEntity> dishEntityList = page.getContent();

        if (dishEntityList.isEmpty()) {
            throw new NoDataFoundException();
        }

        return dishEntityMapper.toDishModelList(dishEntityList);
    }

}
