package com.pragma.mall.infrastructure.out.jpa.adapter;

import com.pragma.mall.domain.model.RestaurantModel;
import com.pragma.mall.domain.spi.IRestaurantPersistencePort;
import com.pragma.mall.infrastructure.exception.NoDataFoundException;
import com.pragma.mall.infrastructure.out.jpa.entity.RestaurantEntity;
import com.pragma.mall.infrastructure.out.jpa.mapper.IRestaurantEntityMapper;
import com.pragma.mall.infrastructure.out.jpa.repository.IRestaurantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;


@RequiredArgsConstructor
public class RestaurantJpaAdapter implements IRestaurantPersistencePort {

    private final IRestaurantRepository restaurantRepository;
    private final IRestaurantEntityMapper restaurantEntityMapper;

    @Override
    public RestaurantModel saveRestaurant(RestaurantModel restaurantModel) {
        RestaurantEntity restaurantEntity = restaurantRepository.save(restaurantEntityMapper.toEntity(restaurantModel));
        return restaurantEntityMapper.toRestaurantModel(restaurantEntity);
    }
    @Override
    public RestaurantModel getById(Long restaurat_id) {
        RestaurantEntity restaurantEntity = restaurantRepository.findById(restaurat_id).orElseThrow(() -> new NoDataFoundException());
        return restaurantEntityMapper.toRestaurantModel(restaurantEntity);
    }

    @Override
    public List<RestaurantModel> getRestaurants(int pageNow, int size) {
        Pageable pagingSort = PageRequest.of(pageNow, size, Sort.by("name"));
        Page<RestaurantEntity> page = restaurantRepository.findAll(pagingSort);
        List<RestaurantEntity> listRestaurantEntity = page.getContent();
        return restaurantEntityMapper.listToRestaurantModel(listRestaurantEntity);
    }
}
