package com.pragma.mall.infrastructure.exception;

public class NoOwnerException extends RuntimeException{
    public NoOwnerException() {
        super();
    }
}
