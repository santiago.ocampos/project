package com.pragma.mall.infrastructure.out.jpa.repository;

import com.pragma.mall.infrastructure.out.jpa.entity.DishEntity;
import com.pragma.mall.infrastructure.out.jpa.entity.OrderEntity;
import feign.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IOrderRepository extends PagingAndSortingRepository<OrderEntity, Long> {
    @Query("SELECT d FROM OrderEntity d WHERE d.status = :status")
    Page<OrderEntity> findAllByOrder(@Param("status") String status, Pageable pageable);

}
