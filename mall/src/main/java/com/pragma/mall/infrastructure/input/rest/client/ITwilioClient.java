package com.pragma.mall.infrastructure.input.rest.client;

import com.pragma.mall.application.dto.reponse.ResponseClientDto;
import com.pragma.mall.application.dto.reponse.ResponseDto;
import com.pragma.mall.application.dto.request.TwilioRequestDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;


@FeignClient(name = "twilio-service", path = "/api/v1/twilio")
public interface ITwilioClient {

    @PostMapping("/")
    public ResponseEntity<ResponseDto> sendMessage(@RequestHeader(value = "Authorization", required = true) String authorizationHeader, TwilioRequestDto twilioRequestDto);

}