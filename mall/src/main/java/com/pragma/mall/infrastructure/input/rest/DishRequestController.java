package com.pragma.mall.infrastructure.input.rest;


import com.pragma.mall.application.dto.reponse.DishCategoryResponseDto;
import com.pragma.mall.application.dto.reponse.DishResponseDto;
import com.pragma.mall.application.dto.reponse.ResponseDto;
import com.pragma.mall.application.dto.request.ListPaginationRequest;
import com.pragma.mall.application.dto.request.dish.DishRequestDto;
import com.pragma.mall.application.dto.request.dish.DishUpdateDto;
import com.pragma.mall.application.handler.IDishHandler;
import com.pragma.mall.infrastructure.exception.NoDataFoundException;
import com.pragma.mall.infrastructure.exception.NoOwnerException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/dish")
@RequiredArgsConstructor
public class DishRequestController {
    private final IDishHandler dishHandler;
    @Operation(summary = "H3 Crear un plato")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Plato Creado", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResponseDto.class)) }),
    })
    @RolesAllowed({"ROLE_PROPIETARIO"})
    @PostMapping("/")
    public ResponseEntity<ResponseDto> saveDish(@Valid @RequestBody DishRequestDto dishRequestDto, BindingResult bindingResult) {

        ResponseDto responseDto=new ResponseDto();

        if (bindingResult.hasErrors()) {
            List<String> errors = bindingResult.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());

            responseDto.setError(true);
            responseDto.setMessage("Error en las validaciones");
            responseDto.setData(errors);

            return ResponseEntity.badRequest().body(responseDto);
        }

        try {
            DishResponseDto dishResponseDto= dishHandler.saveDish(dishRequestDto);

            responseDto.setError(false);
            responseDto.setMessage(null);
            responseDto.setData(dishResponseDto);

        }catch (NoOwnerException ex) {
            responseDto.setError(true);
            responseDto.setMessage("Usted no es propietario de este restaurante");
            responseDto.setData(null);
        }catch (NoDataFoundException ex){
            responseDto.setError(true);
            responseDto.setMessage("Categoria y/o Restaurante no existente");
            responseDto.setData(null);
        }catch (Exception e){
            responseDto.setError(true);
            responseDto.setMessage("Error interno en el servicdor");
            responseDto.setData(null);
        }

        return ResponseEntity.ok(responseDto);
    }


    @Operation(summary = "H4 Modificar un plato")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Plato Modificado", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResponseDto.class)) }),
    })
    @RolesAllowed({"ROLE_PROPIETARIO"})
    @PutMapping("/{id}")
    public ResponseEntity<ResponseDto> updateDish(@Valid @RequestBody DishUpdateDto dishUpdateRequestDto,BindingResult bindingResult,@PathVariable Long id ) {

        ResponseDto responseDto=new ResponseDto();

        if (bindingResult.hasErrors()) {
            List<String> errors = bindingResult.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());

            responseDto.setError(true);
            responseDto.setMessage("Error en las validaciones");
            responseDto.setData(errors);

            return ResponseEntity.badRequest().body(responseDto);
        }

        try {
            DishResponseDto dishResponseDto= dishHandler.updateDish(dishUpdateRequestDto,id);

            responseDto.setError(false);
            responseDto.setMessage(null);
            responseDto.setData(dishResponseDto);

        }catch (NoOwnerException ex) {
            responseDto.setError(true);
            responseDto.setMessage("Usted no es propietario de este restaurante");
            responseDto.setData(null);
        }catch (NoDataFoundException ex){
            responseDto.setError(true);
            responseDto.setMessage("Restaurante inexistente");
            responseDto.setData(null);
        }catch (Exception e){
            responseDto.setError(true);
            responseDto.setMessage("Error interno en el servicdor");
            responseDto.setData(null);
        }

        return ResponseEntity.ok(responseDto);
    }

    @Operation(summary = "H7 Habilitar/deshabilitar un plato")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Plato Creado", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResponseDto.class)) }),
    })
    @RolesAllowed({"ROLE_PROPIETARIO"})
    @PutMapping("/enable/{id}")
    public ResponseEntity<ResponseDto> updateDish(@PathVariable Long id ) {

        ResponseDto responseDto=new ResponseDto();

        try {
            DishResponseDto dishResponseDto= dishHandler.changeStatusDish(id);

            responseDto.setError(false);
            responseDto.setMessage(null);
            responseDto.setData(dishResponseDto);

        }catch (NoDataFoundException ex){
            responseDto.setError(true);
            responseDto.setMessage("Restaurante inexistente");
            responseDto.setData(null);
        }catch (NoOwnerException ex) {
            responseDto.setError(true);
            responseDto.setMessage("Usted no es propietario de este restaurante");
            responseDto.setData(null);
        }
        catch (Exception e){
            responseDto.setError(true);
            responseDto.setMessage("Error interno en el servicdor");
            responseDto.setData(null);
        }

        return ResponseEntity.ok(responseDto);
    }

    @Operation(summary = "H10 Listar los platos de un restaurante")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Lista de platos", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResponseDto.class)) }),
    })
    @RolesAllowed({"ROLE_CLIENTE"})
    @GetMapping("/dishes/{id}")
    public ResponseEntity<ResponseDto> getAllDishesByRestaurantId(@Valid @RequestBody ListPaginationRequest listPaginationRequest,
                                                                  @PathVariable Long id,
                                                                  BindingResult bindingResult) {
        ResponseDto responseDto = new ResponseDto();
        if (bindingResult.hasErrors()) {
            List<String> errors = bindingResult.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());

            responseDto.setError(true);
            responseDto.setMessage("Error en las validaciones");
            responseDto.setData(errors);

            return ResponseEntity.badRequest().body(responseDto);
        }

        try {
            List<DishCategoryResponseDto> dishResponseDtoList = dishHandler.getDishesByRestaurant(listPaginationRequest, id);
            responseDto.setError(false);
            responseDto.setMessage(null);
            responseDto.setData(dishResponseDtoList);

        } catch (NoDataFoundException ex) {
            responseDto.setError(true);
            responseDto.setMessage("No se encontró el restaurante");
            responseDto.setData(null);
            return new ResponseEntity<>(responseDto, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }
}
