package com.pragma.mall.infrastructure.out.jpa.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "dishes")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DishEntity {


    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(length = 50)
    private String name;

    @Column(length = 200)
    private String description;

    @Column
    private float price;


    @Column
    private boolean status;

    @Column(length = 50)
    private String url;

    @Column(length = 50)
    private String file;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_category")
    private CategoryEntity category;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_restaurant")
    private RestaurantEntity restaurant;


}
