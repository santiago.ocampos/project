package com.pragma.mall.infrastructure.exception;

public class NoPinException extends RuntimeException {
    public NoPinException() {
        super();
    }
}
