package com.pragma.mall.infrastructure.out.jpa.adapter;


import com.pragma.mall.domain.spi.IUserPersistencePort;
import com.pragma.mall.infrastructure.out.jpa.mapper.IRestaurantEntityMapper;
import com.pragma.mall.infrastructure.out.jpa.mapper.IUserEntityMapper;
import com.pragma.mall.infrastructure.out.jpa.repository.IRestaurantRepository;
import com.pragma.mall.infrastructure.out.jpa.repository.IUserRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserJpaAdapter implements IUserPersistencePort {

    private final IUserRepository userRepository;
    private final IUserEntityMapper restaurantEntityMapper;

}
