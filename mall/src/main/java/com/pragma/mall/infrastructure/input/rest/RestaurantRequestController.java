package com.pragma.mall.infrastructure.input.rest;


import com.pragma.mall.application.dto.reponse.ResponseDto;
import com.pragma.mall.application.dto.reponse.RestaurantResponseDto;
import com.pragma.mall.application.dto.request.ListPaginationRequest;
import com.pragma.mall.application.dto.request.RestaurantRequestDto;
import com.pragma.mall.application.handler.IRestaurantHandler;
import com.pragma.mall.infrastructure.configuration.FeignClientInterceptor;
import com.pragma.mall.infrastructure.exception.NoDataFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/restaurant")
@RequiredArgsConstructor
public class RestaurantRequestController {

    private final IRestaurantHandler restaurantHandler;

    @Operation(summary = "H2 Crear un restaurante")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Restaurante Creado", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResponseDto.class)) }),
    })
    @RolesAllowed({"ROLE_ADMINISTRADOR"})
    @PostMapping("/")
    public ResponseEntity<ResponseDto> saveRestaurant(@Valid @RequestBody RestaurantRequestDto restaurantRequestDto, BindingResult bindingResult) {

        ResponseDto responseDto=new ResponseDto();

        if (bindingResult.hasErrors()) {
            List<String> errors = bindingResult.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());

            responseDto.setError(true);
            responseDto.setMessage("Error en las validaciones");
            responseDto.setData(errors);

            return ResponseEntity.badRequest().body(responseDto);
        }

        try {
            RestaurantResponseDto restaurantResponseDto= restaurantHandler.saveRestaurant(restaurantRequestDto);
            if(restaurantResponseDto==null){
                responseDto.setError(true);
                responseDto.setMessage("El usuario debe ser propietario");
                responseDto.setData(null);
            }else {
                responseDto.setError(false);
                responseDto.setMessage(null);
                responseDto.setData(restaurantResponseDto);
            }
        }catch (NoDataFoundException ex){
            responseDto.setError(true);
            responseDto.setMessage("Usuario no existente");
            responseDto.setData(null);
        }catch (Exception e){
            responseDto.setError(true);
            responseDto.setMessage("Error interno en el servicdor");
            responseDto.setData(null);
        }

        return ResponseEntity.ok(responseDto);
    }


    @Operation(summary = "H9 Listar los restaurantes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "lista", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResponseDto.class)) }),
    })
    @RolesAllowed({"ROLE_CLIENTE"})
    @GetMapping("/")
    public ResponseEntity<ResponseDto> getRestaurants(@Valid @RequestBody ListPaginationRequest listPaginationRequest,
                                                      BindingResult bindingResult) {

        ResponseDto responseDto=new ResponseDto();
        if (bindingResult.hasErrors()) {
            List<String> errors = bindingResult.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());

            responseDto.setError(true);
            responseDto.setMessage("Error en las validaciones");
            responseDto.setData(errors);

            return ResponseEntity.badRequest().body(responseDto);
        }

        try {
            List<RestaurantResponseDto> restaurantsResponseDto= restaurantHandler.getRestaurants(listPaginationRequest);
            if(restaurantsResponseDto==null){
                responseDto.setError(true);
                responseDto.setMessage("El usuario debe ser propietario");
                responseDto.setData(null);
            }else {
                responseDto.setError(false);
                responseDto.setMessage(null);
                responseDto.setData(restaurantsResponseDto);
            }
        }catch (NoDataFoundException ex){
            responseDto.setError(true);
            responseDto.setMessage("Usuario no existente");
            responseDto.setData(null);
        }catch (Exception e){
            responseDto.setError(true);
            responseDto.setMessage("Error interno en el servicdor");
            responseDto.setData(null);
        }

        return ResponseEntity.ok(responseDto);
    }
}
