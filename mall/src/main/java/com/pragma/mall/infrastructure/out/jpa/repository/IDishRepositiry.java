package com.pragma.mall.infrastructure.out.jpa.repository;

import com.pragma.mall.infrastructure.out.jpa.entity.DishEntity;
import com.pragma.mall.infrastructure.out.jpa.entity.RestaurantEntity;
import feign.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IDishRepositiry extends PagingAndSortingRepository<DishEntity, Long> {
    @Query("SELECT d FROM DishEntity d WHERE d.restaurant.id = :restaurant_id")
    Page<DishEntity> findAllByRestaurantId(@Param("restaurant_id") Long restaurant_id, Pageable pageable);


}
