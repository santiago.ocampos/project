package com.pragma.mall.infrastructure.out.jpa.mapper;

import com.pragma.mall.application.dto.reponse.DishCategoryResponseDto;
import com.pragma.mall.domain.model.*;
import com.pragma.mall.infrastructure.out.jpa.entity.OrderDishEntity;
import com.pragma.mall.infrastructure.out.jpa.entity.OrderEntity;
import com.pragma.mall.infrastructure.out.jpa.entity.RestaurantEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IOrderEntityMapper {
    IOrderEntityMapper INSTANCE = Mappers.getMapper(IOrderEntityMapper.class);
    OrderEntity toEntity(OrderModel order);
    OrderModel toOrderModel(OrderEntity orderEntity);


    @Mapping(target = "order.dishes", ignore = true)
    OrderDishModel toOrderDishModel(OrderDishEntity orderDishEntity);



    List<OrderModel> toOrderModelList(List<OrderEntity> orderEntityList);



    default List<OrderEntity> toOrderModelList2(List<OrderEntity> orderEntityList) {
        return orderEntityList.stream()
                .map(orderEntity -> {
                    List<OrderDishEntity> list=new ArrayList<>();
                    orderEntity.getDishes().stream().map(
                            dish->{
                                dish.setOrder(null);
                                list.add(dish);
                                return null;
                            }
                    ).toList();
                    orderEntity.setDishes(list);


                    return orderEntity;
                }).toList();
    }

    default OrderEntity toOrderModelList3(OrderEntity orderEntity) {
        List<OrderDishEntity> list=new ArrayList<>();
        orderEntity.getDishes().stream()
                .map(dish -> {
                    dish.setOrder(null);
                    list.add(dish);
                    return null;
                }).toList();
        orderEntity.setDishes(list);
        return orderEntity;

    }
}
