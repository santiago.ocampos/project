package com.pragma.mall.infrastructure.out.jpa.mapper;


import com.pragma.mall.domain.model.RestaurantModel;
import com.pragma.mall.infrastructure.out.jpa.entity.RestaurantEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IRestaurantEntityMapper {


    RestaurantEntity toEntity(RestaurantModel restaurant);
    RestaurantModel toRestaurantModel(RestaurantEntity restaurantEntity);

    List<RestaurantModel> listToRestaurantModel(List<RestaurantEntity> restaurantEntity);
}
