package com.pragma.mall.infrastructure.out.jpa.adapter;

import com.pragma.mall.domain.model.DishModel;
import com.pragma.mall.domain.model.OrderDishModel;
import com.pragma.mall.domain.model.OrderModel;
import com.pragma.mall.domain.model.RestaurantModel;
import com.pragma.mall.domain.spi.IOrdenPersistencePort;
import com.pragma.mall.infrastructure.exception.NoDataFoundException;
import com.pragma.mall.infrastructure.out.jpa.entity.DishEntity;
import com.pragma.mall.infrastructure.out.jpa.entity.OrderDishEntity;
import com.pragma.mall.infrastructure.out.jpa.entity.OrderEntity;
import com.pragma.mall.infrastructure.out.jpa.entity.RestaurantEntity;
import com.pragma.mall.infrastructure.out.jpa.mapper.IDishEntityMapper;
import com.pragma.mall.infrastructure.out.jpa.mapper.IOrderEntityMapper;
import com.pragma.mall.infrastructure.out.jpa.repository.IDishRepositiry;
import com.pragma.mall.infrastructure.out.jpa.repository.IOrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

@RequiredArgsConstructor
public class OrderJpaAdapter implements IOrdenPersistencePort {


    private final IOrderRepository orderRepository;

    private final IOrderEntityMapper orderEntityMapper;
    @Override
    public OrderModel saveOrder(OrderModel orderModel) {
        OrderEntity orderEntity = orderRepository.save(orderEntityMapper.toEntity(orderModel));
        return orderEntityMapper.toOrderModel(orderEntity);
    }
    @Override
    public OrderModel getById(Long order_id) {
        OrderEntity orderEntity = orderRepository.findById(order_id).orElseThrow(() -> new NoDataFoundException());
        OrderModel orderModel=orderEntityMapper.toOrderModel(orderEntity);
        return orderModel;
    }
    @Override
    public List<OrderModel> getAllOrdersByStatus(int pageN, int size, String status) {
        Pageable pagingSort = PageRequest.of(pageN, size);
        Page<OrderEntity> page = orderRepository.findAllByOrder(status, pagingSort);
        List<OrderEntity> dishEntityList = page.getContent();


        if (dishEntityList.isEmpty()) {
            throw new NoDataFoundException();
        }
        List<OrderModel> entity=orderEntityMapper.toOrderModelList(dishEntityList);
        return entity;
    }

    @Override
    public OrderModel assignOrder(OrderModel orderModel) {
        OrderEntity orderEntity= orderRepository.save(orderEntityMapper.toEntity(orderModel));
        OrderModel orderModel2=orderEntityMapper.toOrderModel(orderEntity);
        return orderModel2;
    }
}
