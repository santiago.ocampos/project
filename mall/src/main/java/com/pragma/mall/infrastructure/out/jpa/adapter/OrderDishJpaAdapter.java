package com.pragma.mall.infrastructure.out.jpa.adapter;

import com.pragma.mall.application.mapper.IOrderDishRequestMapper;
import com.pragma.mall.domain.model.OrderDishModel;
import com.pragma.mall.domain.spi.IOrdenDishPersistencePort;
import com.pragma.mall.infrastructure.out.jpa.entity.OrderDishEntity;
import com.pragma.mall.infrastructure.out.jpa.mapper.IOrdenDishEntityMapper;
import com.pragma.mall.infrastructure.out.jpa.repository.IOrderDishRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class OrderDishJpaAdapter implements IOrdenDishPersistencePort {

    private final IOrderDishRepository orderDishRepository;

    private final IOrdenDishEntityMapper orderDishEntityMapper;
    @Override
    public OrderDishModel saveOrderDish(OrderDishModel orderDishModel) {
        OrderDishEntity orderDishEntity= orderDishRepository.save(orderDishEntityMapper.toEntity(orderDishModel));
        return orderDishEntityMapper.toOrderDishModel(orderDishEntity);
    }
}
