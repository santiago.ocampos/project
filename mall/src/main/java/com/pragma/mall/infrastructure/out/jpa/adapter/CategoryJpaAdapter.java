package com.pragma.mall.infrastructure.out.jpa.adapter;


import com.pragma.mall.domain.model.CategoryModel;
import com.pragma.mall.domain.spi.ICategoryPersistencePort;
import com.pragma.mall.infrastructure.exception.NoDataFoundException;
import com.pragma.mall.infrastructure.out.jpa.entity.CategoryEntity;
import com.pragma.mall.infrastructure.out.jpa.mapper.ICategoryEntityMapper;
import com.pragma.mall.infrastructure.out.jpa.repository.ICategoryRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class CategoryJpaAdapter implements ICategoryPersistencePort {


    private final ICategoryRepository categoryRepository;
    private final ICategoryEntityMapper categoryEntityMapper;


    @Override
    public CategoryModel getById(Long user_id) {
        CategoryEntity userEntity = categoryRepository.findById(user_id).orElseThrow(() -> new NoDataFoundException());
        return categoryEntityMapper.toCategoryModel(userEntity);
    }

    @Override
    public List<CategoryModel> getAllCategories() {
        List<CategoryEntity> categoryEntityList = categoryRepository.findAll();

        if (categoryEntityList.isEmpty()) {
            throw new NoDataFoundException();
        }

        return categoryEntityMapper.toCategoryModelList(categoryEntityList);
    }
}
