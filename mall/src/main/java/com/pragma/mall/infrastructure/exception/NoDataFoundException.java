package com.pragma.mall.infrastructure.exception;

public class NoDataFoundException extends RuntimeException{

    public NoDataFoundException() {
        super();
    }

}
