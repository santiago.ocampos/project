package com.pragma.mall.infrastructure.out.jpa.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "orders")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class OrderEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    private Long user_id;


    private Date date;

    private Double pin;

    private String status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_restaurant")
    private RestaurantEntity restaurant;

    @OneToMany(mappedBy="order")
    private List<OrderDishEntity> dishes =new ArrayList<>();
}
