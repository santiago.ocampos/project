package com.pragma.mall.infrastructure.out.jpa.mapper;

import com.pragma.mall.domain.model.OrderDishModel;
import com.pragma.mall.domain.model.OrderModel;
import com.pragma.mall.infrastructure.out.jpa.entity.OrderDishEntity;
import com.pragma.mall.infrastructure.out.jpa.entity.OrderEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IOrdenDishEntityMapper {

    @Mappings({
            @Mapping(target = "order.dishes", ignore = true)
    })
    OrderDishEntity toEntity(OrderDishModel order);


    @Mappings({
            @Mapping(target = "order.dishes", ignore = true)
    })
    OrderDishModel toOrderDishModel(OrderDishEntity orderDishEntity);
}
