package com.pragma.mall.application.handler.impl.Factory;

import com.pragma.mall.application.dto.reponse.ResponseClientDto;
import com.pragma.mall.application.dto.request.UserRequestDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class FactoryResponseEntityDataTest {


    public static ResponseEntity<ResponseClientDto> getResponseEntity(){
        ResponseClientDto responseClientDto= new ResponseClientDto();
        responseClientDto.setError(false);
        responseClientDto.setMessage(null);
        responseClientDto.setData(FactoryUserDataTest.getUserRequestDto());
        return ResponseEntity.ok(responseClientDto);
    }
    public static ResponseEntity<ResponseClientDto> getResponseEntityUserNull(){
        ResponseClientDto responseClientDto= new ResponseClientDto();
        responseClientDto.setError(false);
        responseClientDto.setMessage(null);
        responseClientDto.setData(null);
        return ResponseEntity.ok(responseClientDto);
    }
}
