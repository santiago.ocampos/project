package com.pragma.mall.application.handler.impl.Factory;

import com.pragma.mall.application.dto.reponse.OrderDishesResponseDto;
import com.pragma.mall.application.dto.reponse.OrderResponseDto;
import com.pragma.mall.application.dto.request.OrderDishRequestDto;
import com.pragma.mall.domain.model.OrderDishModel;
import com.pragma.mall.domain.model.OrderModel;
import com.pragma.mall.domain.model.RestaurantModel;

import java.util.ArrayList;
import java.util.List;

public class FactoryOrderDishDataTest {

    public static List<OrderDishRequestDto> getOrderDishRequestDtoList(){
        List<OrderDishRequestDto> list= new ArrayList<>();
        OrderDishRequestDto orderDishRequestDto=new OrderDishRequestDto();

        orderDishRequestDto.setAmount(2);
        orderDishRequestDto.setDish_id(1L);
        list.add(orderDishRequestDto);

        return list;
    }
    public static List<OrderDishesResponseDto> getOrderDishResponseDtoList(){
        List<OrderDishesResponseDto> list= new ArrayList<>();
        OrderDishesResponseDto orderDishRequestDto=new OrderDishesResponseDto();

        orderDishRequestDto.setAmount(2);
        orderDishRequestDto.setDish(FactoryDishDataTest.getDishResponseDto());
        list.add(orderDishRequestDto);
        return list;
    }
    public static OrderResponseDto getOrderResponseDto(){
        OrderResponseDto orderResponseDto= new OrderResponseDto();
        orderResponseDto.setDishes(getOrderDishResponseDtoList());

        return orderResponseDto;
    }
    public static OrderDishModel getOrderDishModel(){
        OrderDishModel orderDishModel= new OrderDishModel();
        orderDishModel.setOrder(getOrderModel());
        orderDishModel.setAmount(2);
        orderDishModel.setDish(FactoryDishDataTest.getDishMdel());


        return orderDishModel;
    }
    public static OrderModel getOrderModel(){
        OrderModel orderModel= new OrderModel();
        orderModel.setUser_id(1L);
        orderModel.setDate(null);
        orderModel.setRestaurant(FactoryRestaurantDataTest.getRestaurantModel());
        orderModel.setStatus("PENDIENTE");

        return orderModel;
    }


}
