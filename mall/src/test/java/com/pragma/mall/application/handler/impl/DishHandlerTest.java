package com.pragma.mall.application.handler.impl;

import com.pragma.mall.application.dto.reponse.DishCategoryResponseDto;
import com.pragma.mall.application.dto.reponse.DishResponseDto;
import com.pragma.mall.application.dto.reponse.ResponseClientDto;
import com.pragma.mall.application.dto.reponse.RestaurantResponseDto;
import com.pragma.mall.application.dto.request.ListPaginationRequest;
import com.pragma.mall.application.dto.request.dish.DishRequestDto;
import com.pragma.mall.application.dto.request.dish.DishUpdateDto;
import com.pragma.mall.application.handler.impl.Factory.*;
import com.pragma.mall.application.mapper.IDishRequestMapper;
import com.pragma.mall.application.mapper.IDishResponseMapper;
import com.pragma.mall.domain.api.ICategoryServicePort;
import com.pragma.mall.domain.api.IDishServicePort;
import com.pragma.mall.domain.api.IRestaurantServicePort;
import com.pragma.mall.domain.model.CategoryModel;
import com.pragma.mall.domain.model.DishModel;
import com.pragma.mall.domain.model.RestaurantModel;
import com.pragma.mall.infrastructure.configuration.FeignClientInterceptor;
import com.pragma.mall.infrastructure.input.rest.client.IUserClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
class DishHandlerTest {

    @InjectMocks
    DishHandler dishHandler;
    @Mock
    IDishServicePort dishServicePort;

    @Mock
    ICategoryServicePort categoryServicePort;

    @Mock
    IRestaurantServicePort restaurantServicePort;

    @Mock
    IDishResponseMapper dishResponseMapper;

    @Mock
    IDishRequestMapper dishRequestMapper;

    @Mock
    IUserClient userRepository;



    @Test
    void saveDish() {
        DishRequestDto dishRequestDto= FactoryDishDataTest.getDishRequestDto();
        DishResponseDto dishResponseDto= FactoryDishDataTest.getDishResponseDto();
        DishModel dishModel= FactoryDishDataTest.getDishMdel();
        CategoryModel categoryModel= FactoryCategoryDataTest.getCategoryModel();
        RestaurantModel restaurantModel= FactoryRestaurantDataTest.getRestaurantModel();
        ResponseEntity<ResponseClientDto> responseEntity = FactoryResponseEntityDataTest.getResponseEntity();

        try (MockedStatic<FeignClientInterceptor> utilities = Mockito.mockStatic(FeignClientInterceptor.class)) {
            utilities.when(FeignClientInterceptor::getBearerTokenHeader).thenReturn("");
            Mockito.when(userRepository.getInfo(Mockito.any())).thenReturn(responseEntity);

            Mockito.when(categoryServicePort.getById(Mockito.any())).thenReturn(categoryModel);
            Mockito.when(restaurantServicePort.getById(Mockito.any())).thenReturn(restaurantModel);
            Mockito.when(dishRequestMapper.toDish(Mockito.any())).thenReturn(dishModel);
            Mockito.when(dishResponseMapper.toDto(Mockito.any())).thenReturn(dishResponseDto);

            Assertions.assertEquals(dishResponseDto, this.dishHandler.saveDish(dishRequestDto));

            Mockito.verify(dishServicePort).saveDish(Mockito.any(DishModel.class));


        }


    }

    @Test
    void updateDish() {
        DishUpdateDto dishRequestDto= FactoryDishDataTest.getDishUpdateDto();
        DishModel dishModel= FactoryDishDataTest.getDishMdel();
        DishResponseDto dishResponseDto= FactoryDishDataTest.getDishResponseDto();

        ResponseEntity<ResponseClientDto> responseEntity = FactoryResponseEntityDataTest.getResponseEntity();
        try (MockedStatic<FeignClientInterceptor> utilities = Mockito.mockStatic(FeignClientInterceptor.class)) {
            utilities.when(FeignClientInterceptor::getBearerTokenHeader).thenReturn("");
            Mockito.when(userRepository.getInfo(Mockito.any())).thenReturn(responseEntity);
            Mockito.when(dishServicePort.getById(Mockito.any())).thenReturn(dishModel);
            Mockito.when(dishRequestMapper.toUpdateDish(Mockito.any())).thenReturn(dishModel);
            Mockito.when(dishResponseMapper.toDto(Mockito.any())).thenReturn(dishResponseDto);
            Assertions.assertEquals(dishResponseDto, this.dishHandler.updateDish(dishRequestDto,1L));
            Mockito.verify(dishServicePort).updateDish(Mockito.any(DishModel.class),Mockito.any(Long.class));
        }
    }


    @Test
    void enableDish() {
        DishModel dishModel= FactoryDishDataTest.getDishMdel();
        DishResponseDto dishResponseDto= FactoryDishDataTest.getDishResponseDto();
        ResponseEntity<ResponseClientDto> responseEntity = FactoryResponseEntityDataTest.getResponseEntity();
        try (MockedStatic<FeignClientInterceptor> utilities = Mockito.mockStatic(FeignClientInterceptor.class)) {
            utilities.when(FeignClientInterceptor::getBearerTokenHeader).thenReturn("");
            Mockito.when(userRepository.getInfo(Mockito.any())).thenReturn(responseEntity);
            Mockito.when(dishServicePort.getById(Mockito.any())).thenReturn(dishModel);
            Mockito.when(dishResponseMapper.toDto(Mockito.any())).thenReturn(dishResponseDto);
            Assertions.assertEquals(dishResponseDto, this.dishHandler.changeStatusDish(1L));
            Mockito.verify(dishServicePort).updateDish(Mockito.any(DishModel.class),Mockito.any(Long.class));
        }
    }

    @Test
    void getDishesByRestaurant() {
        ListPaginationRequest listPaginationRequest = FactoryListPaginationRequestTest.getListPaginationRequest();
        List<DishCategoryResponseDto> listRestaurantResponseDto = FactoryDishDataTest.getDishByRestaurants();

        Mockito.when(dishResponseMapper.toDishCategoryList(Mockito.any(),Mockito.any())).thenReturn(listRestaurantResponseDto);

        Assertions.assertEquals(listRestaurantResponseDto, this.dishHandler.getDishesByRestaurant(listPaginationRequest,1L));

        Mockito.verify(dishServicePort).getAllDishesByRestaurant(listPaginationRequest.getPageN(),listPaginationRequest.getSize(),1L);
        Mockito.verify(categoryServicePort).getAllCategories();



    }


}