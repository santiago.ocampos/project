package com.pragma.mall.application.handler.impl.Factory;

import com.pragma.mall.application.dto.reponse.RestaurantResponseDto;
import com.pragma.mall.application.dto.request.RestaurantRequestDto;
import com.pragma.mall.domain.model.RestaurantModel;

import java.util.ArrayList;
import java.util.List;

public class FactoryRestaurantDataTest {


    public static RestaurantModel getRestaurantModel(){
        RestaurantModel restaurantModelExpected= new RestaurantModel();
        restaurantModelExpected.setId(1L);
        restaurantModelExpected.setName("Q rico");
        restaurantModelExpected.setAddress("Cra 22 #17-08");
        restaurantModelExpected.setPhone("78766898");
        restaurantModelExpected.setUrl("www.qrico.com");
        restaurantModelExpected.setNit("9877998");
        restaurantModelExpected.setUser_id(1L);
        return restaurantModelExpected;
    }

    public static RestaurantRequestDto getRestaurantRequestDto(){
        RestaurantRequestDto restaurantRequestDto= new RestaurantRequestDto();
        restaurantRequestDto.setName("Q rico");
        restaurantRequestDto.setAddress("Cra 22 #17-08");
        restaurantRequestDto.setPhone("78766898");
        restaurantRequestDto.setUrl("www.qrico.com");
        restaurantRequestDto.setNit("9877998");
        restaurantRequestDto.setUser_id(1L);
        return restaurantRequestDto;
    }

    public static RestaurantResponseDto getRestaurantResponseDto(){
        RestaurantResponseDto restaurantResponseDto= new RestaurantResponseDto();
        restaurantResponseDto.setId(1L);
        restaurantResponseDto.setName("Q rico");
        restaurantResponseDto.setAddress("Cra 22 #17-08");
        restaurantResponseDto.setPhone("78766898");
        restaurantResponseDto.setUrl("www.qrico.com");
        restaurantResponseDto.setNit("9877998");
        restaurantResponseDto.setUser_id(1L);
        return restaurantResponseDto;
    }

    public static List<RestaurantResponseDto> getRestaurantListResponseDto(){
        List<RestaurantResponseDto> list= new ArrayList<>();
        RestaurantResponseDto restaurantResponseDto= new RestaurantResponseDto();
        restaurantResponseDto.setId(1L);
        restaurantResponseDto.setName("Q rico");
        restaurantResponseDto.setAddress("Cra 22 #17-08");
        restaurantResponseDto.setPhone("78766898");
        restaurantResponseDto.setUrl("www.qrico.com");
        restaurantResponseDto.setNit("9877998");
        restaurantResponseDto.setUser_id(1L);
        list.add(restaurantResponseDto);
        return list;
    }
}
