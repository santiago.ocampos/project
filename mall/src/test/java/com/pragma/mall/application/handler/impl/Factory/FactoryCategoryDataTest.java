package com.pragma.mall.application.handler.impl.Factory;

import com.pragma.mall.application.dto.reponse.CategoryResponseDto;
import com.pragma.mall.domain.model.CategoryModel;

public class FactoryCategoryDataTest {

    public static CategoryModel getCategoryModel(){
        CategoryModel categoryModelExpected= new CategoryModel();
        categoryModelExpected.setId(1L);
        categoryModelExpected.setName("Admin");
        categoryModelExpected.setDescription("Un administrador");
        return categoryModelExpected;
    }
    public static CategoryResponseDto getCategoryResponseDto(){
        CategoryResponseDto categoryResponseDto= new CategoryResponseDto();
        categoryResponseDto.setId(1L);
        categoryResponseDto.setName("Admin");
        categoryResponseDto.setDescription("Un administrador");
        return categoryResponseDto;
    }
}
