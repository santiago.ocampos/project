package com.pragma.mall.application.handler.impl.Factory;

import com.pragma.mall.application.dto.request.ListPaginationRequest;

public class FactoryListPaginationRequestTest {

    public static ListPaginationRequest getListPaginationRequest(){
        ListPaginationRequest listPaginationRequest= new ListPaginationRequest();
        listPaginationRequest.setPageN(0);
        listPaginationRequest.setSize(2);
        return listPaginationRequest;
    }

}
