package com.pragma.mall.application.handler.impl.Factory;

import com.pragma.mall.application.dto.reponse.DishCategoryResponseDto;
import com.pragma.mall.application.dto.reponse.DishResponseDto;
import com.pragma.mall.application.dto.request.RestaurantRequestDto;
import com.pragma.mall.application.dto.request.dish.DishRequestDto;
import com.pragma.mall.application.dto.request.dish.DishUpdateDto;
import com.pragma.mall.domain.model.DishModel;

import java.util.ArrayList;
import java.util.List;

public class FactoryDishDataTest {

    public static DishRequestDto getDishRequestDto(){
        DishRequestDto dishRequestDto= new DishRequestDto();
        dishRequestDto.setName("Hamburguesa");
        dishRequestDto.setDescription("Hamburguesa tradicional");
        dishRequestDto.setPrice(10000F);
        dishRequestDto.setUrl("www.qrico.com");
        dishRequestDto.setFile("file.png");
        dishRequestDto.setCategory_id(1L);
        dishRequestDto.setRestaurant_id(1L);
        return dishRequestDto;
    }

    public static DishUpdateDto getDishUpdateDto(){
        DishUpdateDto dishRequestDto= new DishUpdateDto();
        dishRequestDto.setDescription("Hamburguesa tradicional");
        dishRequestDto.setPrice(10000F);
        return dishRequestDto;
    }

    public static DishResponseDto getDishResponseDto(){
        DishResponseDto dishResponseDto= new DishResponseDto();
        dishResponseDto.setName("Hamburguesa");
        dishResponseDto.setDescription("Hamburguesa tradicional");
        dishResponseDto.setPrice(10000F);
        dishResponseDto.setUrl("www.qrico.com");
        dishResponseDto.setFile("file.png");
        dishResponseDto.setCategory(FactoryCategoryDataTest.getCategoryResponseDto());
        dishResponseDto.setRestaurant(FactoryRestaurantDataTest.getRestaurantResponseDto());
        return dishResponseDto;
    }
    public static DishModel getDishMdel(){
        DishModel dishModel= new DishModel();
        dishModel.setId(1L);
        dishModel.setName("Hamburguesa");
        dishModel.setDescription("Hamburguesa tradicional");
        dishModel.setPrice(10000F);
        dishModel.setUrl("www.qrico.com");
        dishModel.setFile("file.png");
        dishModel.setCategory(FactoryCategoryDataTest.getCategoryModel());
        dishModel.setRestaurant(FactoryRestaurantDataTest.getRestaurantModel());
        return dishModel;
    }

    public static List<DishCategoryResponseDto> getDishByRestaurants(){
        List<DishCategoryResponseDto> list= new ArrayList<>();
        DishCategoryResponseDto dishCategoryResponseDto=new DishCategoryResponseDto();
        dishCategoryResponseDto.setName("categoria 1");
        dishCategoryResponseDto.setDescription("descripcion");
        DishResponseDto dishResponseDto=getDishResponseDto();
        List<DishResponseDto> dishResponseDtos= new ArrayList<>();
        dishResponseDtos.add(dishResponseDto);
        dishCategoryResponseDto.setDishes(dishResponseDtos);
        return list;
    }


}
