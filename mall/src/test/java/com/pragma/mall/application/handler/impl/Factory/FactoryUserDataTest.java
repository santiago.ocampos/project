package com.pragma.mall.application.handler.impl.Factory;

import com.pragma.mall.application.dto.request.RestaurantRequestDto;
import com.pragma.mall.application.dto.request.RoleRequestDto;
import com.pragma.mall.application.dto.request.UserRequestDto;

public class FactoryUserDataTest {

    public static UserRequestDto getUserRequestDto(){
        UserRequestDto userRequestDto= new UserRequestDto();
        userRequestDto.setId(1L);
        userRequestDto.setName("Santiago");
        userRequestDto.setLast_name("Ocampo");
        userRequestDto.setCellphone("78766898");
        userRequestDto.setEmail("santiago.ocampo1108@hotmail.com");
        userRequestDto.setPassword("12345");
        userRequestDto.setRole(getRoleRequestDto());
        return userRequestDto;
    }

    public static RoleRequestDto getRoleRequestDto(){
        RoleRequestDto roleRequestDto= new RoleRequestDto();
        roleRequestDto.setId(1L);
        roleRequestDto.setName("Administrador");
        roleRequestDto.setDescription("Es un administrador");
        return roleRequestDto;
    }
}
