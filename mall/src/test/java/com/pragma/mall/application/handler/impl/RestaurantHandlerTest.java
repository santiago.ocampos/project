package com.pragma.mall.application.handler.impl;

import com.pragma.mall.application.dto.reponse.ResponseClientDto;
import com.pragma.mall.application.dto.reponse.RestaurantResponseDto;
import com.pragma.mall.application.dto.request.ListPaginationRequest;
import com.pragma.mall.application.dto.request.RestaurantRequestDto;
import com.pragma.mall.application.handler.impl.Factory.FactoryListPaginationRequestTest;
import com.pragma.mall.application.handler.impl.Factory.FactoryResponseEntityDataTest;
import com.pragma.mall.application.handler.impl.Factory.FactoryRestaurantDataTest;
import com.pragma.mall.application.mapper.IRestaurantRequestMapper;
import com.pragma.mall.domain.api.IRestaurantServicePort;
import com.pragma.mall.domain.model.RestaurantModel;
import com.pragma.mall.infrastructure.configuration.FeignClientInterceptor;
import com.pragma.mall.infrastructure.exception.NoDataFoundException;
import com.pragma.mall.infrastructure.input.rest.client.IUserClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


@ExtendWith(SpringExtension.class)
class RestaurantHandlerTest {


    @InjectMocks
    RestaurantHandler restaurantHandler;

    @Mock
    IRestaurantServicePort restaurantServicePort;

    @Mock
    IRestaurantRequestMapper restaurantRequestMapper;


    @Mock
    IUserClient userRepository;


    @Test
    void saveRestaurant() {
        RestaurantRequestDto restaurantRequestDto = FactoryRestaurantDataTest.getRestaurantRequestDto();
        RestaurantResponseDto restaurantResponseDto = FactoryRestaurantDataTest.getRestaurantResponseDto();
        RestaurantModel restaurantModel = FactoryRestaurantDataTest.getRestaurantModel();
        ResponseEntity<ResponseClientDto> responseEntity = FactoryResponseEntityDataTest.getResponseEntity();
        try (MockedStatic<FeignClientInterceptor> utilities = Mockito.mockStatic(FeignClientInterceptor.class)) {
            utilities.when(FeignClientInterceptor::getBearerTokenHeader).thenReturn("");
            Mockito.when(restaurantRequestMapper.toRestaurant(Mockito.any())).thenReturn(restaurantModel);
            Mockito.when(userRepository.getUserById(Mockito.any(),Mockito.any())).thenReturn(responseEntity);

            Mockito.when(restaurantRequestMapper.toDto(Mockito.any())).thenReturn(restaurantResponseDto);

            Assertions.assertEquals(restaurantResponseDto, this.restaurantHandler.saveRestaurant(restaurantRequestDto));

            Mockito.verify(restaurantServicePort).saveRestaurant(Mockito.any(RestaurantModel.class));
        }


    }


    @Test
    void saveRestaurantUserNull() {
        RestaurantRequestDto restaurantRequestDto = FactoryRestaurantDataTest.getRestaurantRequestDto();
        RestaurantModel restaurantModel = FactoryRestaurantDataTest.getRestaurantModel();
        ResponseEntity<ResponseClientDto> responseEntity = FactoryResponseEntityDataTest.getResponseEntityUserNull();
        try (MockedStatic<FeignClientInterceptor> utilities = Mockito.mockStatic(FeignClientInterceptor.class)) {
            utilities.when(FeignClientInterceptor::getBearerTokenHeader).thenReturn("");
            Mockito.when(restaurantRequestMapper.toRestaurant(Mockito.any())).thenReturn(restaurantModel);
            Mockito.when(userRepository.getUserById(Mockito.any(), Mockito.any())).thenReturn(responseEntity);


            Assertions.assertThrows(
                    NoDataFoundException.class,
                    () -> {
                        this.restaurantHandler.saveRestaurant(restaurantRequestDto);
                    }
            );
        }

    }
    @Test
    void getRestaurants() {
        ListPaginationRequest listPaginationRequest = FactoryListPaginationRequestTest.getListPaginationRequest();
        List<RestaurantResponseDto> listRestaurantResponseDto = FactoryRestaurantDataTest.getRestaurantListResponseDto();

            Mockito.when(restaurantRequestMapper.listToDto(Mockito.any())).thenReturn(listRestaurantResponseDto);

            Assertions.assertEquals(listRestaurantResponseDto, this.restaurantHandler.getRestaurants(listPaginationRequest));

            Mockito.verify(restaurantServicePort).getRestaurants(listPaginationRequest.getPageN(),listPaginationRequest.getSize());



    }
}