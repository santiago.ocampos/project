package com.pragma.mall.application.handler.impl;

import com.pragma.mall.application.dto.reponse.OrderResponseDto;
import com.pragma.mall.application.dto.reponse.ResponseClientDto;
import com.pragma.mall.application.dto.reponse.RestaurantResponseDto;
import com.pragma.mall.application.dto.request.OrderDishRequestDto;
import com.pragma.mall.application.dto.request.RestaurantRequestDto;
import com.pragma.mall.application.handler.impl.Factory.FactoryDishDataTest;
import com.pragma.mall.application.handler.impl.Factory.FactoryOrderDishDataTest;
import com.pragma.mall.application.handler.impl.Factory.FactoryResponseEntityDataTest;
import com.pragma.mall.application.handler.impl.Factory.FactoryRestaurantDataTest;
import com.pragma.mall.application.mapper.IOrderDishRequestMapper;
import com.pragma.mall.application.mapper.IOrderDishResponseMapper;
import com.pragma.mall.application.mapper.IRestaurantRequestMapper;
import com.pragma.mall.domain.api.IDishServicePort;
import com.pragma.mall.domain.api.IOrderDishServicePort;
import com.pragma.mall.domain.api.IOrderServicePort;
import com.pragma.mall.domain.api.IRestaurantServicePort;
import com.pragma.mall.domain.model.DishModel;
import com.pragma.mall.domain.model.OrderDishModel;
import com.pragma.mall.domain.model.OrderModel;
import com.pragma.mall.domain.model.RestaurantModel;
import com.pragma.mall.infrastructure.configuration.FeignClientInterceptor;
import com.pragma.mall.infrastructure.input.rest.client.IUserClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class OrderHandlerTest {
    @InjectMocks
    OrderHandler orderHandler;


    @Mock
    IRestaurantServicePort restaurantServicePort;

    @Mock
    IDishServicePort dishServicePort;


    @Mock
    IOrderServicePort orderServicePort;


    @Mock
    IOrderDishServicePort orderDishServicePort;



    @Mock
    IOrderDishRequestMapper orderDishRequestMapper;

    @Mock
    IOrderDishResponseMapper orderDishResponseMapper;

    @Mock
    IUserClient userRepository;


    @Test
    void saveOrder() {
        List<OrderDishRequestDto> orderDishRequestDtos= FactoryOrderDishDataTest.getOrderDishRequestDtoList();
        RestaurantModel restaurantModel = FactoryRestaurantDataTest.getRestaurantModel();
        OrderResponseDto orderResponseDto=FactoryOrderDishDataTest.getOrderResponseDto();
        DishModel dishModel= FactoryDishDataTest.getDishMdel();
        OrderDishModel orderDishModel=FactoryOrderDishDataTest.getOrderDishModel();
        ResponseEntity<ResponseClientDto> responseEntity = FactoryResponseEntityDataTest.getResponseEntity();
        try (MockedStatic<FeignClientInterceptor> utilities = Mockito.mockStatic(FeignClientInterceptor.class)) {
            utilities.when(FeignClientInterceptor::getBearerTokenHeader).thenReturn("");
            Mockito.when(userRepository.getInfo(Mockito.any())).thenReturn(responseEntity);
            Mockito.when(restaurantServicePort.getById(Mockito.any())).thenReturn(restaurantModel);
            Mockito.when(dishServicePort.getById(Mockito.any())).thenReturn(dishModel);
            Mockito.when(orderDishRequestMapper.toOrderDish(Mockito.any())).thenReturn(orderDishModel);
            Mockito.when(orderDishResponseMapper.toDishCategoryList(Mockito.any(),Mockito.any())).thenReturn(orderResponseDto);

            Assertions.assertEquals(orderResponseDto, this.orderHandler.saveOrder(orderDishRequestDtos,1L));

            Mockito.verify(orderServicePort).saveOrder(Mockito.any(OrderModel.class));
            Mockito.verify(orderDishServicePort).saveOrderDish(Mockito.any(OrderDishModel.class));

        }


    }
    @Test
    void assignOrder() {

        OrderResponseDto orderResponseDto=FactoryOrderDishDataTest.getOrderResponseDto();

        ResponseEntity<ResponseClientDto> responseEntity = FactoryResponseEntityDataTest.getResponseEntity();
        try (MockedStatic<FeignClientInterceptor> utilities = Mockito.mockStatic(FeignClientInterceptor.class)) {
            utilities.when(FeignClientInterceptor::getBearerTokenHeader).thenReturn("");
            Mockito.when(userRepository.getInfo(Mockito.any())).thenReturn(responseEntity);
            Mockito.when(orderDishResponseMapper.toOrderModelToOrderResponse(Mockito.any())).thenReturn(orderResponseDto);

            Assertions.assertEquals(orderResponseDto, this.orderHandler.assignOrder(1L,12.0));

            //Mockito.verify(orderServicePort).assignOrder();

        }


    }
}