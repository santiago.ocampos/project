package com.pragma.users.application.dto.reponse;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserResponseDto {

    private Long id;


    private String name;


    private String last_name;


    private String cellphone;


    private String email;

    private RoleResponseDto role;
}
