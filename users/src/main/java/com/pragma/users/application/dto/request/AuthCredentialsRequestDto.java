package com.pragma.users.application.dto.request;


import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@Builder
@Getter
@Setter
public class AuthCredentialsRequestDto {

    @Email(message ="Digite correctamente el correo electronico")
    private String email;

    @NotNull(message ="El campo contraseña es obligatorio")
    private String password;
}
