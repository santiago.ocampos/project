package com.pragma.users.application.handler;

import com.pragma.users.application.dto.reponse.RoleResponseDto;
import com.pragma.users.application.dto.request.RoleRequestDto;
import com.pragma.users.domain.model.RoleModel;

public interface IRoleHandler {

    void saveRole(RoleRequestDto roleRequestDto);

}
