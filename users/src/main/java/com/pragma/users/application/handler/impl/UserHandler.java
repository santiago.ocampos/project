package com.pragma.users.application.handler.impl;


import com.pragma.users.application.dto.reponse.AuthResponseDto;
import com.pragma.users.application.dto.reponse.UserResponseDto;
import com.pragma.users.application.dto.request.AuthCredentialsRequestDto;
import com.pragma.users.application.dto.request.UserRequestDto;
import com.pragma.users.application.handler.IJwtHandler;
import com.pragma.users.application.handler.IUserHandler;
import com.pragma.users.application.mapper.IUserRequestMapper;
import com.pragma.users.domain.api.IRoleServicePort;
import com.pragma.users.domain.api.IUserServicePort;
import com.pragma.users.domain.model.RoleModel;
import com.pragma.users.domain.model.UserModel;
import com.pragma.users.infrastructure.out.jpa.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class UserHandler implements IUserHandler {
    private final IUserServicePort userServicePort;
    private final IUserRequestMapper userRequestMapper;

    private final IRoleServicePort roleServicePort;

    private final IJwtHandler jwtHandler;

    private final AuthenticationManager authenticationManager;

    @Override
    public UserResponseDto saveUserOwner(UserRequestDto userRequestDto) {
        RoleModel role=roleServicePort.getById(2L);
        UserModel userModel = userRequestMapper.toUser(userRequestDto);
        userModel.setRole(role);
        return userRequestMapper.toDto(userServicePort.saveUserOwner(userModel));
    }

    @Override
    public UserResponseDto saveUserEmployee(UserRequestDto userRequestDto) {
        RoleModel role=roleServicePort.getById(3L);
        UserModel userModel = userRequestMapper.toUser(userRequestDto);
        userModel.setRole(role);
        return userRequestMapper.toDto(userServicePort.saveUserOwner(userModel));
    }

    @Override
    public UserResponseDto saveUserClient(UserRequestDto userRequestDto) {
        RoleModel role=roleServicePort.getById(4L);
        UserModel userModel = userRequestMapper.toUser(userRequestDto);
        userModel.setRole(role);
        return userRequestMapper.toDto(userServicePort.saveUserOwner(userModel));
    }
    @Override
    public UserResponseDto getById(Long user_id) {
        return userRequestMapper.toDto(userServicePort.getById(user_id));
    }

    @Override
    public UserResponseDto getByEmail(String email) {
        return userRequestMapper.toDto(userServicePort.findOneByEmailExtern(email));
    }


    @Override
    public AuthResponseDto login(AuthCredentialsRequestDto authenticationRequestDto) {
        AuthResponseDto jwtResponseDto = new AuthResponseDto();

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authenticationRequestDto.getEmail(),
                        authenticationRequestDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserEntity user = userServicePort.findOneByEmail(authenticationRequestDto.getEmail());
        String jwtToken = jwtHandler.generateToken(user);



        jwtResponseDto.setToken(jwtToken);
        jwtResponseDto.setBearer("Bearer ");
        jwtResponseDto.setEmail(user.getUsername());
        jwtResponseDto.setAuthorities(user.getAuthorities());

        return jwtResponseDto;
    }

    @Override
    public UserResponseDto me(Authentication authentication) {
        return userRequestMapper.toDto(userServicePort.me(authentication));
    }


}
