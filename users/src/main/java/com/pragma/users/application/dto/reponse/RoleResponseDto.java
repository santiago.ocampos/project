package com.pragma.users.application.dto.reponse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleResponseDto {

    private long id;
    private String name;
    private String description;
}
