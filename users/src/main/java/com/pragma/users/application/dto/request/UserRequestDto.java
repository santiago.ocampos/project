package com.pragma.users.application.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter
public class UserRequestDto {


    @NotNull(message ="El campo nombre es obligatorio")
    private String name;


    @NotNull(message ="El campo apellido es obligatorio")
    private String last_name;


    @NotNull(message ="El campo celular es obligatorio")
    @Pattern(regexp = "[+]?\\d+(\\d+)?", message="EL celular debe ser numerico")
    @Size(max=13, message="El  celular no puede pasar de 13 caracteres")
    private String cellphone;


    @NotNull(message ="El campo correo electronico es obligatorio")
    @Email(message ="Digite correctamente el correo electronico")
    private String email;


    @NotNull(message ="El campo contraseña es obligatorio")
    private String password;
}
