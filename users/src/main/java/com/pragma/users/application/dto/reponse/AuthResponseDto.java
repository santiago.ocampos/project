package com.pragma.users.application.dto.reponse;


import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Getter
@Setter
public class AuthResponseDto {

    private String token;
    private String bearer;
    private String email;
    private Collection<? extends GrantedAuthority> authorities;

}
