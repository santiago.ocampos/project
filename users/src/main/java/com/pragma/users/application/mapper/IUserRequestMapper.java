package com.pragma.users.application.mapper;

import com.pragma.users.application.dto.reponse.UserResponseDto;
import com.pragma.users.application.dto.request.UserRequestDto;
import com.pragma.users.domain.model.UserModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IUserRequestMapper {

    UserModel toUser(UserRequestDto userRequestDto);

    UserResponseDto toDto(UserModel userModel);

}
