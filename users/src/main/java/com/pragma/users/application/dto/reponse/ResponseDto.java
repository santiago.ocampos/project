package com.pragma.users.application.dto.reponse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseDto {
    private boolean error;
    private String message;
    private Object data;
}
