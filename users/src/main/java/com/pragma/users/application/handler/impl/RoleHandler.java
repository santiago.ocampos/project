package com.pragma.users.application.handler.impl;

import com.pragma.users.application.dto.reponse.RoleResponseDto;
import com.pragma.users.application.dto.request.RoleRequestDto;
import com.pragma.users.application.handler.IRoleHandler;
import com.pragma.users.application.mapper.IRoleRequestMapper;
import com.pragma.users.application.mapper.IRoleResponseMapper;
import com.pragma.users.domain.api.IRoleServicePort;
import com.pragma.users.domain.model.RoleModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
@Transactional
public class RoleHandler implements IRoleHandler {

    private final IRoleServicePort roleServicePort;
    private final IRoleRequestMapper roleRequestMapper;

    @Override
    public void saveRole(RoleRequestDto roleRequestDto) {
        RoleModel roleModel = roleRequestMapper.toRole(roleRequestDto);
        roleServicePort.saveRole(roleModel);
    }

}
