package com.pragma.users.application.handler;

import com.pragma.users.application.dto.reponse.AuthResponseDto;
import com.pragma.users.application.dto.reponse.UserResponseDto;
import com.pragma.users.application.dto.request.AuthCredentialsRequestDto;
import com.pragma.users.application.dto.request.UserRequestDto;
import org.springframework.security.core.Authentication;

public interface IUserHandler {

    UserResponseDto saveUserOwner(UserRequestDto userRequestDto);


    UserResponseDto saveUserEmployee(UserRequestDto userRequestDto);

    UserResponseDto saveUserClient(UserRequestDto userRequestDto);

    UserResponseDto getById(Long user_id);

    UserResponseDto getByEmail(String email);

    AuthResponseDto login(AuthCredentialsRequestDto authenticationRequestDto);
    UserResponseDto me(Authentication authentication);
}
