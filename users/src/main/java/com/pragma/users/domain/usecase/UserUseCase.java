package com.pragma.users.domain.usecase;

import com.pragma.users.domain.api.IUserServicePort;
import com.pragma.users.domain.model.UserModel;
import com.pragma.users.domain.spi.IUserPersistencePort;
import com.pragma.users.infrastructure.out.jpa.entity.UserEntity;
import org.springframework.security.core.Authentication;

public class UserUseCase implements IUserServicePort {

    private final IUserPersistencePort userPersistencePort;

    public UserUseCase(IUserPersistencePort userPersistencePort) {
        this.userPersistencePort = userPersistencePort;
    }

    @Override
    public UserModel saveUserOwner(UserModel userModel) {
        return userPersistencePort.saveUserOwner(userModel);
    }


    @Override
    public UserModel getById(Long user_id) {
        return userPersistencePort.getById(user_id);
    }

    @Override
    public UserEntity findOneByEmail(String email) {
        return userPersistencePort.findOneByEmail(email);
    }

    @Override
    public UserModel findOneByEmailExtern(String email) {
        return userPersistencePort.findOneByEmailExtern(email);
    }

    @Override
    public UserModel me(Authentication authentication) {
        return userPersistencePort.me(authentication);
    }
}


