package com.pragma.users.domain.api;

import com.pragma.users.domain.model.RoleModel;

public interface IRoleServicePort {
    RoleModel saveRole(RoleModel roleModel);

    RoleModel getById(Long role_id);

}
