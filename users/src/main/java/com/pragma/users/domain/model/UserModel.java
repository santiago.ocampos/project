package com.pragma.users.domain.model;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserModel {

    private Long id;


    private String name;


    private String last_name;


    private String cellphone;


    private String email;


    private String password;

    private RoleModel role;
}
