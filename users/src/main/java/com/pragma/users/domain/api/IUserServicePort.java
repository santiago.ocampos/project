package com.pragma.users.domain.api;

import com.pragma.users.application.dto.reponse.UserResponseDto;
import com.pragma.users.domain.model.UserModel;
import com.pragma.users.infrastructure.out.jpa.entity.UserEntity;
import org.springframework.security.core.Authentication;

public interface IUserServicePort {

    UserModel saveUserOwner(UserModel userModel);

    UserModel getById(Long user_id);


    UserEntity findOneByEmail(String email);


    UserModel findOneByEmailExtern(String email);

    UserModel me(Authentication authentication);
}
