package com.pragma.users.domain.usecase;

import com.pragma.users.domain.api.IRoleServicePort;
import com.pragma.users.domain.model.RoleModel;
import com.pragma.users.domain.spi.IRolePersistencePort;

public class RoleUseCase implements IRoleServicePort {

    private final IRolePersistencePort rolePersistencePort;

    public RoleUseCase(IRolePersistencePort rolePersistencePort) {
        this.rolePersistencePort = rolePersistencePort;
    }

    @Override
    public RoleModel saveRole(RoleModel roleModel) {
        return rolePersistencePort.saveRole(roleModel);
    }


    @Override
    public RoleModel getById(Long role_id) {
        return rolePersistencePort.getById(role_id);
    }
}
