package com.pragma.users.domain.spi;

import com.pragma.users.domain.model.RoleModel;

public interface IRolePersistencePort {
    RoleModel saveRole(RoleModel roleModel);

    RoleModel getById(Long role_id);
}
