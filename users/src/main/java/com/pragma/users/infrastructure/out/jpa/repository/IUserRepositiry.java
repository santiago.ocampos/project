package com.pragma.users.infrastructure.out.jpa.repository;

import com.pragma.users.infrastructure.out.jpa.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IUserRepositiry extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findByEmail(String email);
}
