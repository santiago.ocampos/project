package com.pragma.users.infrastructure.input.rest;


import com.pragma.users.application.dto.reponse.ResponseDto;
import com.pragma.users.application.dto.request.AuthCredentialsRequestDto;
import com.pragma.users.application.handler.IUserHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthenticationManager authenticationManager;

    private final IUserHandler userHandler;

    @Operation(summary = "H6 Añadir autenticación")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Sesión Iniciada", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResponseDto.class)) }),
    })
    @PostMapping("/login")
    public ResponseEntity<ResponseDto> login(@Valid @RequestBody AuthCredentialsRequestDto authCredentialsRequestDto, BindingResult bindingResult){
        ResponseDto responseDto=new ResponseDto();

        if (bindingResult.hasErrors()) {
            List<String> errors = bindingResult.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());

            responseDto.setError(true);
            responseDto.setMessage("Error en las validaciones");
            responseDto.setData(errors);

            return ResponseEntity.badRequest().body(responseDto);
        }

        responseDto.setError(false);
        responseDto.setMessage(null);
        responseDto.setData( userHandler.login(authCredentialsRequestDto));


        return ResponseEntity.ok(responseDto);
    }
}
