package com.pragma.users.infrastructure.input.rest;

import com.pragma.users.application.dto.request.RoleRequestDto;
import com.pragma.users.application.handler.IRoleHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/role")
@RequiredArgsConstructor
public class RoleRequestController {


    private final IRoleHandler roleHandler;

    @Operation(summary = "Add a new role")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "role created", content = @Content),
            @ApiResponse(responseCode = "409", description = "role already exists", content = @Content)
    })
    @PostMapping("/")
    public ResponseEntity<Void> saveRole(@RequestBody RoleRequestDto roleRequestDto) {
        roleHandler.saveRole(roleRequestDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}