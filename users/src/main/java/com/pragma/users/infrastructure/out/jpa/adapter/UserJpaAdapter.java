package com.pragma.users.infrastructure.out.jpa.adapter;

import com.pragma.users.application.handler.IJwtHandler;
import com.pragma.users.domain.model.UserModel;
import com.pragma.users.domain.spi.IUserPersistencePort;
import com.pragma.users.infrastructure.exception.NoDataFoundException;
import com.pragma.users.infrastructure.out.jpa.entity.RoleEntity;
import com.pragma.users.infrastructure.out.jpa.entity.UserEntity;
import com.pragma.users.infrastructure.out.jpa.mapper.IUserEntityMapper;
import com.pragma.users.infrastructure.out.jpa.repository.IUserRepositiry;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;

@RequiredArgsConstructor
public class UserJpaAdapter implements IUserPersistencePort {


    @Autowired
    private PasswordEncoder passwordEncoder;
    private final IUserRepositiry userRepository;
    private final IUserEntityMapper userEntityMapper;

    private final IJwtHandler jwtHandler;

    @Override
    public UserModel saveUserOwner(UserModel userModel) {
        UserEntity userEntity = userRepository.save(userEntityMapper.toEntity(userModel));
        userEntity.setPassword(passwordEncoder.encode(userModel.getPassword()));
        String jwtToken = jwtHandler.generateToken(userEntity);
        return userEntityMapper.toUserModel(userEntity);
    }
    @Override
    public UserModel getById(Long user_id) {
        UserEntity userEntity = userRepository.findById(user_id).orElseThrow(() -> new NoDataFoundException());
        return userEntityMapper.toUserModel(userEntity);
    }
    @Override
    public UserEntity findOneByEmail(String email) {
        UserEntity userEntity = userRepository.findByEmail(email).orElseThrow(() -> new NoDataFoundException());
        return userEntity;
    }

    @Override
    public UserModel findOneByEmailExtern(String email) {
        UserEntity userEntity = userRepository.findByEmail(email).orElseThrow(() -> new NoDataFoundException());
        return userEntityMapper.toUserModel(userEntity);
    }

    @Override
    public UserModel me(Authentication authentication) {
        UserEntity userEntity = (UserEntity) authentication.getPrincipal();

        return userEntityMapper.toUserModel(userEntity);
    }
}
