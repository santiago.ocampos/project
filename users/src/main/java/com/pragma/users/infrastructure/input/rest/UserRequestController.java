package com.pragma.users.infrastructure.input.rest;


import com.pragma.users.application.dto.reponse.ResponseClientDto;
import com.pragma.users.application.dto.reponse.ResponseDto;
import com.pragma.users.application.dto.reponse.UserResponseDto;
import com.pragma.users.application.dto.request.UserRequestDto;
import com.pragma.users.application.handler.IUserHandler;
import com.pragma.users.infrastructure.exception.NoDataFoundException;
import com.pragma.users.infrastructure.out.jpa.entity.UserEntity;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.*;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class UserRequestController {

    private final IUserHandler userHandler;

    @Operation(summary = "H1 Crear propietario")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "H1 - Propiertario Creado", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResponseDto.class)) }),
    })

    @PostMapping("/owner")
    @RolesAllowed({"ROLE_ADMINISTRADOR"})
    public ResponseEntity<ResponseDto> saveUserOwner(@Valid @RequestBody UserRequestDto userRequestDto, BindingResult bindingResult) {
        ResponseDto responseDto=new ResponseDto();
        if (bindingResult.hasErrors()) {
            List<String> errors = bindingResult.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());

            responseDto.setError(true);
            responseDto.setMessage("Error en las validaciones");
            responseDto.setData(errors);

            return ResponseEntity.badRequest().body(responseDto);
        }

        try {
            responseDto.setError(false);
            responseDto.setMessage(null);
            responseDto.setData( userHandler.saveUserOwner(userRequestDto));
        }catch (NoDataFoundException ex){
            responseDto.setError(true);
            responseDto.setMessage("Usuario No encontrado");
            responseDto.setData(null);
        }catch (Exception e){
            responseDto.setError(true);
            responseDto.setMessage("Error interno en el servicdor");
            responseDto.setData(null);
        }


        return ResponseEntity.ok(responseDto);
    }

    @Operation(summary = "H6 Crear Empleado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "H6 - Empleado Creado", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResponseDto.class)) }),
    })

    @PostMapping("/employee")
    @RolesAllowed({"ROLE_PROPIETARIO"})
    public ResponseEntity<ResponseDto> saveUseremployee(@Valid @RequestBody UserRequestDto userRequestDto, BindingResult bindingResult) {
        ResponseDto responseDto=new ResponseDto();
        if (bindingResult.hasErrors()) {
            List<String> errors = bindingResult.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());

            responseDto.setError(true);
            responseDto.setMessage("Error en las validaciones");
            responseDto.setData(errors);

            return ResponseEntity.badRequest().body(responseDto);
        }

        try {
            responseDto.setError(false);
            responseDto.setMessage(null);
            responseDto.setData( userHandler.saveUserEmployee(userRequestDto));
        }catch (NoDataFoundException ex){
            responseDto.setError(true);
            responseDto.setMessage("Usuario No encontrado");
            responseDto.setData(null);
        }catch (Exception e){
            responseDto.setError(true);
            responseDto.setMessage("Error interno en el servicdor");
            responseDto.setData(null);
        }

        return ResponseEntity.ok(responseDto);
    }

    @Operation(summary = "H8 Crear Cliente")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "H8 - Cliente Creado", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResponseDto.class)) }),
    })

    @PostMapping("/client")
    public ResponseEntity<ResponseDto> saveUserCLient(@Valid @RequestBody UserRequestDto userRequestDto, BindingResult bindingResult) {
        ResponseDto responseDto=new ResponseDto();
        if (bindingResult.hasErrors()) {
            List<String> errors = bindingResult.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());

            responseDto.setError(true);
            responseDto.setMessage("Error en las validaciones");
            responseDto.setData(errors);

            return ResponseEntity.badRequest().body(responseDto);
        }

        try {
            responseDto.setError(false);
            responseDto.setMessage(null);
            responseDto.setData( userHandler.saveUserClient(userRequestDto));
        }catch (NoDataFoundException ex){
            responseDto.setError(true);
            responseDto.setMessage("Usuario No encontrado");
            responseDto.setData(null);
        }catch (Exception e){
            responseDto.setError(true);
            responseDto.setMessage("Error interno en el servicdor");
            responseDto.setData(null);
        }

        return ResponseEntity.ok(responseDto);
    }





    @GetMapping("/{id}")
    public ResponseEntity<ResponseClientDto> getUserById(@PathVariable Long id) {
        ResponseClientDto responseDto=new ResponseClientDto();
        try {
            UserResponseDto userResponseDto=userHandler.getById(id);
            responseDto.setError(false);
            responseDto.setMessage(null);
            responseDto.setData( userResponseDto);
        }catch (NoDataFoundException ex){
            responseDto.setError(true);
            responseDto.setMessage("Usuario No encontrado");
            responseDto.setData(null);
        }catch (Exception e){
            responseDto.setError(true);
            responseDto.setMessage("Error interno en el servicdor");
            responseDto.setData(null);
        }

        return ResponseEntity.ok(responseDto);
    }


    @GetMapping("/me")
    public ResponseEntity<ResponseDto> getInfo(Authentication authentication){
        ResponseDto responseDto=new ResponseDto();

        try {
            UserResponseDto userResponseDto=userHandler.me(authentication);
            responseDto.setError(false);
            responseDto.setMessage(null);
            responseDto.setData( userResponseDto);
        }catch (NoDataFoundException ex){
            responseDto.setError(true);
            responseDto.setMessage("Usuario No encontrado");
            responseDto.setData(null);
        }catch (Exception e){
            responseDto.setError(true);
            responseDto.setMessage("Error interno en el servicdor");
            responseDto.setData(null);
        }

        return ResponseEntity.ok(responseDto);
    }
    @GetMapping("/email/{email}")
    public ResponseEntity<ResponseClientDto> getUserByEmail(@PathVariable String email) {
        ResponseClientDto responseDto=new ResponseClientDto();
        try {
            UserResponseDto userResponseDto=userHandler.getByEmail(email);
            responseDto.setError(false);
            responseDto.setMessage(null);
            responseDto.setData( userResponseDto);
        }catch (NoDataFoundException ex){
            responseDto.setError(true);
            responseDto.setMessage("Usuario No encontrado");
            responseDto.setData(null);
        }catch (Exception e){
            responseDto.setError(true);
            responseDto.setMessage("Error interno en el servicdor");
            responseDto.setData(null);
        }

        return ResponseEntity.ok(responseDto);
    }
}
