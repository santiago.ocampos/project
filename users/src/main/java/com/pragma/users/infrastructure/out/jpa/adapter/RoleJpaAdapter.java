package com.pragma.users.infrastructure.out.jpa.adapter;

import com.pragma.users.domain.model.RoleModel;
import com.pragma.users.domain.spi.IRolePersistencePort;
import com.pragma.users.infrastructure.exception.NoDataFoundException;
import com.pragma.users.infrastructure.out.jpa.entity.RoleEntity;
import com.pragma.users.infrastructure.out.jpa.mapper.IRoleEntityMapper;
import com.pragma.users.infrastructure.out.jpa.repository.IRoleRepository;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class RoleJpaAdapter implements IRolePersistencePort {


    private final IRoleRepository roleRepository;
    private final IRoleEntityMapper roleEntityMapper;
    @Override
    public RoleModel saveRole(RoleModel roleModel) {
        RoleEntity toleEntity = roleRepository.save(roleEntityMapper.toEntity(roleModel));
        return roleEntityMapper.toRoleModel(toleEntity);
    }

    @Override
    public RoleModel getById(Long role_id) {
        RoleEntity roleEntity = roleRepository.findById(role_id).orElseThrow(() -> new NoDataFoundException());
        return roleEntityMapper.toRoleModel(roleEntity);
    }
}
