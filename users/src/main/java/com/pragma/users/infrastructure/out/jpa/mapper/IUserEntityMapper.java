package com.pragma.users.infrastructure.out.jpa.mapper;


import com.pragma.users.domain.model.UserModel;
import com.pragma.users.infrastructure.out.jpa.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IUserEntityMapper {


    UserEntity toEntity(UserModel user);
    UserModel toUserModel(UserEntity userEntity);
}
