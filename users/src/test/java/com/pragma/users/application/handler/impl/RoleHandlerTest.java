package com.pragma.users.application.handler.impl;

import com.pragma.users.application.dto.request.RoleRequestDto;
import com.pragma.users.application.handler.impl.Factory.FactoryRoleDataTest;
import com.pragma.users.application.mapper.IRoleRequestMapper;
import com.pragma.users.domain.api.IRoleServicePort;
import com.pragma.users.domain.model.RoleModel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(SpringExtension.class)
class RoleHandlerTest {

    @InjectMocks
    RoleHandler roleHandler;


    @Mock
    IRoleServicePort roleServicePort;

    @Mock
    IRoleRequestMapper roleRequestMapper;
    @Test
    void mustSaveARole() {
        RoleRequestDto roleRequestDto = FactoryRoleDataTest.getRoleRequestDto();

        RoleModel roleModelExpected= FactoryRoleDataTest.getRoleModel();

        Mockito.when(roleRequestMapper.toRole(Mockito.any())).thenReturn(roleModelExpected);
        roleHandler.saveRole(roleRequestDto);
        Mockito.verify(roleServicePort).saveRole(Mockito.any(RoleModel.class));
    }

}