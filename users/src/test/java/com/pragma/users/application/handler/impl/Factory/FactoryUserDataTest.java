package com.pragma.users.application.handler.impl.Factory;

import com.pragma.users.application.dto.reponse.UserResponseDto;
import com.pragma.users.application.dto.request.UserRequestDto;
import com.pragma.users.domain.model.RoleModel;
import com.pragma.users.domain.model.UserModel;

public class FactoryUserDataTest {
    public static UserResponseDto getUserResponseDto(){
        UserResponseDto userResponseDto=new UserResponseDto();
        userResponseDto.setId(1L);
        userResponseDto.setName("Santiago");
        userResponseDto.setLast_name("Ocampo");
        userResponseDto.setCellphone("3188230963");
        userResponseDto.setEmail("santiago.ocampo1108@gmail.com");
        userResponseDto.setRole(FactoryRoleDataTest.getRoleResponseDto());
        return userResponseDto;

    }

    public static UserRequestDto getUserRequestDto(){
        UserRequestDto userRequestDto=new UserRequestDto();
        userRequestDto.setName("Santiago");
        userRequestDto.setLast_name("Ocampo");
        userRequestDto.setCellphone("3188230963");
        userRequestDto.setEmail("santiago.ocampo1108@gmail.com");
        return userRequestDto;

    }

    public static UserModel getUserModel(){
        UserModel userModel=new UserModel();
        userModel.setId(1L);
        userModel.setName("Santiago");
        userModel.setLast_name("Ocampo");
        userModel.setCellphone("3188230963");
        userModel.setEmail("santiago.ocampo1108@gmail.com");
        userModel.setRole(FactoryRoleDataTest.getRoleModel());
        return userModel;

    }


}
