package com.pragma.users.application.handler.impl;

import com.pragma.users.application.dto.reponse.UserResponseDto;
import com.pragma.users.application.dto.request.UserRequestDto;
import com.pragma.users.application.handler.impl.Factory.FactoryRoleDataTest;
import com.pragma.users.application.handler.impl.Factory.FactoryUserDataTest;
import com.pragma.users.application.mapper.IUserRequestMapper;
import com.pragma.users.domain.api.IRoleServicePort;
import com.pragma.users.domain.api.IUserServicePort;
import com.pragma.users.domain.model.RoleModel;
import com.pragma.users.domain.model.UserModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
class UserHandlerTest {

    @InjectMocks
    UserHandler userHandler;
    @Mock
    IUserServicePort userServicePort;


    @Mock
    IUserRequestMapper userRequestMapper;


    @Mock
    IRoleServicePort roleServicePort;


    @Test
    void saveUserOwner() {


        UserRequestDto userRequestDto= FactoryUserDataTest.getUserRequestDto();
        UserResponseDto expectedUser= FactoryUserDataTest.getUserResponseDto();
        UserModel userModel= FactoryUserDataTest.getUserModel();
        RoleModel roleModel= FactoryRoleDataTest.getRoleModel();
        Mockito.when(userRequestMapper.toUser(Mockito.any())).thenReturn(userModel);
        Mockito.when(roleServicePort.getById(Mockito.any())).thenReturn(roleModel);
        Mockito.when(userRequestMapper.toDto(Mockito.any())).thenReturn(expectedUser);
        Assertions.assertEquals(expectedUser, this.userHandler.saveUserOwner(userRequestDto));

        Mockito.verify(userServicePort).saveUserOwner(Mockito.any(UserModel.class));

    }

    @Test
    void saveUserEmployee() {


        UserRequestDto userRequestDto= FactoryUserDataTest.getUserRequestDto();
        UserResponseDto expectedUser= FactoryUserDataTest.getUserResponseDto();
        UserModel userModel= FactoryUserDataTest.getUserModel();
        RoleModel roleModel= FactoryRoleDataTest.getRoleModel();
        Mockito.when(userRequestMapper.toUser(Mockito.any())).thenReturn(userModel);
        Mockito.when(roleServicePort.getById(Mockito.any())).thenReturn(roleModel);
        Mockito.when(userRequestMapper.toDto(Mockito.any())).thenReturn(expectedUser);
        Assertions.assertEquals(expectedUser, this.userHandler.saveUserEmployee(userRequestDto));

        Mockito.verify(userServicePort).saveUserOwner(Mockito.any(UserModel.class));

    }
    @Test
    void saveUserClient() {
        UserRequestDto userRequestDto= FactoryUserDataTest.getUserRequestDto();
        UserResponseDto expectedUser= FactoryUserDataTest.getUserResponseDto();
        UserModel userModel= FactoryUserDataTest.getUserModel();
        RoleModel roleModel= FactoryRoleDataTest.getRoleModel();
        Mockito.when(userRequestMapper.toUser(Mockito.any())).thenReturn(userModel);
        Mockito.when(roleServicePort.getById(Mockito.any())).thenReturn(roleModel);
        Mockito.when(userRequestMapper.toDto(Mockito.any())).thenReturn(expectedUser);
        Assertions.assertEquals(expectedUser, this.userHandler.saveUserClient(userRequestDto));

        Mockito.verify(userServicePort).saveUserOwner(Mockito.any(UserModel.class));

    }

    @Test
    void getById() {
        Long userId= 1L;
        UserResponseDto expectedUser= FactoryUserDataTest.getUserResponseDto();
        UserModel userModel= FactoryUserDataTest.getUserModel();
        Mockito.when(userServicePort.getById(Mockito.any())).thenReturn(userModel);
        Mockito.when(userRequestMapper.toDto(Mockito.any())).thenReturn(expectedUser);
        Assertions.assertEquals(expectedUser, this.userHandler.getById(userId));
    }
}