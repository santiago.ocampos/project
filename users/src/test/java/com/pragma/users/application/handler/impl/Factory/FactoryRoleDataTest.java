package com.pragma.users.application.handler.impl.Factory;

import com.pragma.users.application.dto.reponse.RoleResponseDto;
import com.pragma.users.application.dto.request.RoleRequestDto;
import com.pragma.users.domain.model.RoleModel;

public class FactoryRoleDataTest {

    public static RoleRequestDto getRoleRequestDto(){
        RoleRequestDto roleRequestDto =new RoleRequestDto();
        roleRequestDto.setName("Admin");
        roleRequestDto.setDescription("Un administrador");
        return roleRequestDto;
    }


    public static RoleResponseDto getRoleResponseDto(){
        RoleResponseDto roleResponseDto =new RoleResponseDto();
        roleResponseDto.setId(1L);
        roleResponseDto.setName("Admin");
        roleResponseDto.setDescription("Un administrador");
        return roleResponseDto;
    }

    public static RoleModel getRoleModel(){
        RoleModel roleModelExpected= new RoleModel();
        roleModelExpected.setId(1L);
        roleModelExpected.setName("Admin");
        roleModelExpected.setDescription("Un administrador");
        return roleModelExpected;
    }
}
